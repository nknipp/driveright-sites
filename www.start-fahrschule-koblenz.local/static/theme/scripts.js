(function ($) {
  'use strict'

  jQuery(document).ready(function () {

    /* Preloader */
    $(window).on('load', function () {
      $('.preloader').delay(800).fadeOut('slow')
    })

    /* Smooth Scroll */
    $('a.smoth-scroll').on("click", function (e) {
      var anchor = $(this)
      $('html, body').stop().animate({
        scrollTop: $(anchor.attr('href')).offset().top - 50
      }, 1000)
      e.preventDefault()
    })

    /* Mobile Navigation Hide or Collapse on Click */
    $(document).on('click', '.navbar-collapse.in', function (e) {
      if ($(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle') {
        $(this).collapse('hide')
      }
    })
    $('body').scrollspy({
      target: '.navbar-collapse',
      offset: 195
    })

    /* Scroll To Top */
    $(window).scroll(function () {
      if ($(this).scrollTop() >= 500) {
        $('.scroll-to-top').fadeIn()
      } else {
        $('.scroll-to-top').fadeOut()
      }
    })

    $('.scroll-to-top').click(function () {
      $('html, body').animate({scrollTop: 0}, 800)
      return false
    })

    new WOW().init()
  })
})(jQuery)