'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import Carousel from '../../../components/carousel'
import RegisterBtn from '../../../components/register-btn'

import OfficesBgImg from '../images/hintergrund-unterseite-filialen.jpg'
import Office1Img from '../images/filiale.jpg'
import Office2Img from '../images/filiale-2.jpg'
import Office3Img from '../images/filiale-3.jpg'

const Meta = () =>
  <Helmet>
    <title>Start Fahrschule Koblenz - Wir sind Koblenz!</title>
    <meta name="description" content="Die Start Fahrschule Koblenz ist in Koblenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Koblenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${OfficesBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Wir sind Koblenz!</h1>
              <p>Du findest uns im Zentrum von Koblenz, um genau zu sein am Moselring 1! In unserer Filiale sind wir während unseren Öffnungszeiten gerne für Dich und deine Fragen da! Gerne kannst Du uns aber auch deine Anfrage über unsere Webseite senden.</p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


const CarouselSection = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="row" style={{ paddingBottom: '50px' }}>
        <div className="col-md-6 col-md-offset-3 col-xs-12">
          <div className="about-me-text">
            <Carousel>
              <div className="item">
                <img className="img-responsive" src={Office1Img}/>
              </div>
              <div className="item">
                <img className="img-responsive" src={Office2Img}/>
              </div>
              <div className="item">
                <img className="img-responsive" src={Office3Img}/>
              </div>
            </Carousel>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-6 col-md-offset-3 col-xs-12">
          <p>Bei uns finden regelmäßige Intensivkurse statt, das bedeutet, dass Du innerhalb von sieben Werktagen Deinen kompletten Theoriestoff absolvieren kannst und fit für die Theorieprüfung und die praktische Ausbildung bist.</p>
          <p>Solltest Du Interesse haben an dem Kurs teilzunehmen, freuen wir uns selbstverständlich auf Deine Anfrage. In Kürze findest Du zudem die Möglichkeit unsere Kurse direkt über unsere Webseite zu buchen!</p>
        </div>
      </div>
      <div className="row text-center">
        <RegisterBtn/>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <CarouselSection/>
  </div>
