'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import RegisterBtn from '../../../components/register-btn'

import CampaignParticipants from "../components/campaignParticipants"

import ParentsBgImg from '../images/hintergrund-unterseite-karnevals-aktion.jpg'

const Meta = () =>
  <Helmet>
    <title>Start Fahrschule Koblenz - Mama, ich werde bald 18 und bin erwachsen...</title>
    <meta name="description" content="Die Start Fahrschule Koblenz ist in Koblenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Koblenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${ParentsBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1> Ein Jeck ohne Führerschein,<br/>ist wie ein Fisch ohne Fahrrad.... </h1>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Content = () =>
  <section>
    <div className="container">
      <div className="content">
        <p>
          In Kooperation mit dem Karnevalsverein "Boomer Ritter" haben wir uns für die kommende Session etwas ganz besonderes ausgedacht - wir verlosen
          START-Guthaben für Führerscheinausbildung in unserem Hause im Wert von bis zu 1.274 Euro.<br/>
          1. Preis: START-Guthaben im Wert von 1.274 Euro<br/>
          2. Preis: START-Guthaben im Wert von 764 Euro<br/>
          3. Preis: START-Guthaben im Wert von 514 Euro<br/>
          4. Preis: START-Guthaben im Wert von 364 Euro<br/>
          5. Preis: START-Guthaben im Wert von 295 Euro<br/>
          6 - 10. Preis: START-Guthaben im Wert von 100 Euro
        </p>
        <p>
          <strong>Wie geht das?</strong><br/>Bei verschiedenen Karnevalsveranstaltungen können die "Pins" der Boomer Ritter käuflich erworben werden. Auf der Rückseite
          der beiliegenden Visitenkarte von uns, findest Du eine Losnummer, mit der Du Dich HIER registrierst.
        </p>
        <p>
          Die Aktion endet am Dienstag, 05. März 2019! Die Auslosung findet an Aschermittwoch (06.03.2019) statt. Die Gewinner werden telefonisch benachrichtigt.
        </p>
        <p>
          <strong>Achtung!</strong> Bei Abholung des Gewinns muss das entsprechende Los mitgebracht werden.
        </p>
        <p>
          Der Teilnehmer erklärt sich einverstanden, dass sein Name im Fall des Gewinns auf unserer Facebook-Fanseite veröffentlicht wird.
        </p>
        <p>
          Wir wünschen allen eine tolle närrische Zeit!
        </p>
        <p style={{marginBottom: '70px'}}>
          Eure Start-Fahrschule Koblenz
        </p>
        <CampaignParticipants />
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Content/>
  </div>
