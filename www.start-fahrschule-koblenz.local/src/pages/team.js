'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import Team from '../../../components/team'
import RegisterBtn from '../../../components/register-btn'

import TeamBgImg from '../images/hintergrund-unterseite-team.jpg'

const Meta = () =>
  <Helmet>
    <title>Start Fahrschule Koblenz - Wir sind die START Fahrschule Koblenz</title>
    <meta name="description" content="Die Start Fahrschule Koblenz ist in Koblenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Koblenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${TeamBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Wir sind die START Fahrschule Koblenz</h1>
              <p>Jeden Tag gibt das Team der START Fahrschule Koblenz alles für euren Erfolg und zukünftige Unabhängigkeit. Dafür ist uns kein Aufwand zu groß. Hier möchten wir uns euch kurz und knapp vorstellen</p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Content = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="row">
        <div>
          <Team cssClass="about-me-text" style={{ paddingBottom: '100px' }} />
        </div>
        <div className="col-md-12 text-center">
          <RegisterBtn/>
        </div>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Content/>
  </div>
