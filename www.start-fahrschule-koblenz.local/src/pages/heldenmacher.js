'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import ContactRequest from '../../../components/contact-request'

import DrivingInstructorBgImg from '../images/hintergrund-unterseite-bkf.jpg'
import FunkspotAudio from '../audio/funkspot-recruiting.mp3'

const Meta = () =>
  <Helmet>
    <title>Start Fahrschule Koblenz - Heldenmacher gesucht</title>
    <meta name="description" content="Die Start Fahrschule Koblenz ist in Koblenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Koblenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${DrivingInstructorBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Heldenmacher gesucht!</h1>
              <p>Du bist Fahrlehrer/in und möchtest Dich verändern? Du suchst nach einer neuen Herausforderung? Dann bist Du bei uns genau richtig! Wir suchen genau Dich zur Verstärkung unseres Teams.</p>
              <p>
                <audio controls>
                  <source type="audio/mpeg" src={`${FunkspotAudio}`}/>
                  Ihr Browser kann dieses Tondokument nicht wiedergeben.
                </audio>
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Content = () =>
  <section>
    <div className="container">
      <div className="row content">
        <p>Was uns ausmacht, was wir Dir bieten können und welche Erwartungen wir an Dich stellen, das können wir gerne in einem persönlichen Gespräch besprechen.</p>
        <p>Du bist noch keine/e Fahrlehrer/in, kannst Dir aber vorstellen genau diesen Beruf auszuüben?  Gerne unterstützen wir Dich und stehen Dir als Ausbildungsfahrschule zur Verfügung.</p>
        <p>Telefonisch erreichst Du uns unter der Nummer 015253900204 (auch per WhatsApp) oder hinterlasse uns HIER Deine Nachricht.</p>
        <p>Ansprechpartnerin ist Frau Alexandra Löhr.</p>
        <p>Wir freuen uns auf DICH!</p>
        <div className="col-sm-12 col-md-6">
          <ContactRequest/>
        </div>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Content/>
  </div>
