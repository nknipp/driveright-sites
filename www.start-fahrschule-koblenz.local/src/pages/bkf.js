'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import Courses from '../../../components/courses'
import RegisterBtn from '../../../components/register-btn'

import BkfBgImg from '../images/hintergrund-unterseite-bkf.jpg'

const Meta = () =>
  <Helmet>
    <title>Start Fahrschule Koblenz - Aus- und Weiterbildungen im BKF-Bereich</title>
    <meta name="description" content="Die Start Fahrschule Koblenz ist in Koblenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Koblenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${BkfBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Aus- und Weiterbildungen im BKF-Bereich?!</h1>
              <p>Die START Fahrschule Koblenz bietet hochwertige Kurse für BKF an. Auf dieser Seite finden Sie unsere Kurse. Sollten Sie trotzdem noch Fragen zu unseren Leistungen haben, freuen wir uns auf Ihre Anfrage.</p>
              <RegisterBtn title="Fragen zur unseren BKF Leistungen?"/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Intro = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="row">
        <div className="col-xs-12 text-center">
          <h3>Hier findest Du den passenden Kurs in ...</h3>
        </div>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Intro/>
    <Courses tags="BKF"/>
  </div>
