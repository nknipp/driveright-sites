'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import PrivacyStatement from '../../../components/privacy-statement'

const Meta = () =>
  <Helmet>
    <title>Start Fahrschule Koblenz - Unsere Datenschutzbestimmungen</title>
    <meta name="description" content="Die Start Fahrschule Koblenz ist in Koblenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Koblenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

export default () =>
  <div>
    <Meta/>
    <PrivacyStatement/>
  </div>
