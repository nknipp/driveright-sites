'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import RegisterBtn from '../../../components/register-btn'

import BenefitsBgImg from '../images/hintergrund-unterseite-vorteile.jpg'

const Meta = () =>
  <Helmet>
    <title>Start Fahrschule Koblenz - Viele Gründe, um Deinen Führerschein bei uns zu machen</title>
    <meta name="description" content="Die Start Fahrschule Koblenz ist in Koblenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Koblenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${BenefitsBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Viele Gründe, um Deinen Führerschein bei uns zu machen:</h1>
              <p>Du informierst Dich, dass ist gut! Denn der Führerschein ist nicht nur ein notwendiger Schritt um die eigene Unabhängigkeit zu starten sondern auch eine große Verantwortung. Check doch einfach mal die Vorteile die Du bei einer Führerscheinausbildung bei uns hast! Solltest Du noch Fragen haben, stehen wir Dir gerne zur Verfügung.</p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Content = () =>
  <section className="benefits">
    <div className="container">
      <div className="row">
        <div className="benefits-container">
          <div className="col-md-6 col-sm-12 col-xs-12">
            <div className="benefits-item wow fadeInUp" data-wow-duration=".5s">
              <div className="benefits-circle">
                <i className="fa fa-magic"></i>
              </div>
              <div className="benefits-content">
                <h4>Mentalcoaching</h4>
                <p>FAHRANGST war gestern! Mit uns fährst Du ANGSTfrei!</p>
                <p>Ob Angst vor der Theorie oder der Praxis (Prüfungsangst), ob Trauma nach einem Unfall oder “plötzlich scheinbar ohne Grund da” - wir helfen weiter!</p>
                <p>Unser Fahrtrainer Marco Willems ist nicht “Fahrlehrer” sondern auch Hypnose-Coach und Mentaltrainer und führt diese “Anti-Angst-Trainings” druch.</p>              </div>
            </div>
          </div>
          <div className="col-md-6 col-sm-12 col-xs-12">
            <div className="benefits-item wow fadeInUp" data-wow-duration=".8s">
              <div className="benefits-circle">
                <i className="fa fa-desktop"></i>
              </div>
              <div className="benefits-content">
                <h4>Easy-START-Fahrsimulator</h4>
                <p>Durch den Fahrsimulator hast Du die Möglichkeit, schon vor Deiner ersten Fahrstunde im Fahrschulwagen die ersten “Schritte” des Autofahrenlernens zu machen - ganz entspannt, in Ruhe und Du bestimmst das Tempo! Das gibt Dir für Deine erste Fahrstunde Sicherheit und vor allem Gelassenheit!</p>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-sm-12 col-xs-12">
            <div className="benefits-item wow fadeInUp" data-wow-duration="1.2">
              <div className="benefits-circle">
                <i className="fa fa-video-camera"></i>
              </div>
              <div className="benefits-content">
                <h4>Praxis-App</h4>
                <p>Wir haben unser Prüfgebiet für Dich verfilmen lassen und die kniffligsten Stellen in einer App zusammengestellt. So kannst Du ganz bequem von zu Hause aus Dich auf diese Situationen einstellen!</p>              </div>
            </div>
          </div>
          <div className="col-md-6 col-sm-12 col-xs-12">
            <div className="benefits-item wow fadeInUp" data-wow-duration="1.5s">
              <div className="benefits-circle">
                <i className="fa fa-rocket"></i>
              </div>
              <div className="benefits-content">
                <h4>Intensivkurse</h4>
                <p>Du hast keine Lust auf gezogenen Theorieunterricht? Keine Lust auf eine „never-ending-Führerscheinstory“? Wir lieben KOMPAKT! Unterricht innerhalb 7 Werktagen, Führerscheinausbildung innerhalb 14 Tage oder ein Ausbildungsplan individuell auf Dich zugeschnitten – das zeichnet uns aus!</p>              </div>
            </div>
          </div>
          <div className="col-md-6 col-sm-12 col-xs-12">
            <div className="benefits-item wow fadeInUp" data-wow-duration="1.9s">
              <div className="benefits-circle">
                <i className="fa fa-expeditedssl"></i>
              </div>
              <div className="benefits-content">
                <h4>Theorie Garantie</h4>
                <p>Solltest du trotz unserer speziellen Vorprüfung die Theorieprüfung nicht bestehen, übernehmen wir den Fahrschulbetrag für die nächste Theorieprüfung!</p>              </div>
            </div>
          </div>
          <div className="col-md-6 col-sm-12 col-xs-12">
            <div className="benefits-item wow fadeInUp" data-wow-duration="3.2s">
              <div className="benefits-circle">
                <i className="fa fa-arrows-alt"></i>
              </div>
              <div className="benefits-content">
                <h4>Freie Fahrlehrerwahl</h4>
                <p>Weil Du uns wichtig bist: Bei uns kannst Du Dir deinen Fahrlehrer aussuchen denn nur in einer guten Atmosphäre lässt es sich gut lernen!</p>              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Content/>
  </div>
