'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import Cars from '../../../components/cars'
import RegisterBtn from '../../../components/register-btn'

import CarsBgImg from '../images/hintergrund-unterseite-fahrzeuge.jpg'

const Meta = () =>
  <Helmet>
    <title>Start Fahrschule Koblenz - START cruising.</title>
    <meta name="description" content="Die Start Fahrschule Koblenz ist in Koblenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Koblenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${CarsBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>START cruising.</h1>
              <p>Seit kurzem haben wir unsere Fahrzeugflotte erneuert und auf aktuellen Stand gebracht. Deine Führerscheinausbildung bei der START Fahrschule Koblenz findet in modernen Ausbildungsfahrzeuge der Marke Audi statt. Selbstverständlich werden alle unsere Ausbildungsfahrzeuge regelmäßig gecheckt und gewartet!</p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Content = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="row">
        <Cars/>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Content/>
  </div>

