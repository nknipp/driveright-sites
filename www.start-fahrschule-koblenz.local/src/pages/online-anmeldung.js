'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import OnlineRegistration from '../../../components/online-registration'

import OnlineRegistrationBgImg from '../images/hintergrund-unterseite-kontakt.jpg'

const Meta = () =>
  <Helmet>
    <title>Start Fahrschule Koblenz - Jetzt wird es ernst! Komm vorbei, ruf uns an oder schreibe uns!</title>
    <meta name="description" content="Die Start Fahrschule Koblenz ist in Koblenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Koblenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

export default () =>
  <div>
    <Meta/>
    <div className="welcome-header" style={{ backgroundImage: `url(${OnlineRegistrationBgImg})` }}>
      <div className="container heading-text" style={{ color: '#fff' }}>
        <div className="row">
          <h1>Jetzt können wir STARTen</h1>
          <p>Vielen Dank für Deine Online-Anmeldung. Damit wir für Dich einen Platz in Deinem Wunschkurs reservieren können, bitten wir Dich um folgende Angaben</p>
        </div>
        <div className="row" style={{ paddingTop: '40px', paddingBottom: '40px' }}>
          <div className="col-sm-12 col-md-12">
            <OnlineRegistration/>
          </div>
        </div>
      </div>
    </div>
  </div>
