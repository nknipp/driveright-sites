'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import Impressum from '../../../components/impressum'

const Meta = () =>
  <Helmet>
    <title>Start Fahrschule Koblenz - Impressum</title>
    <meta name="description" content="Die Start Fahrschule Koblenz ist in Koblenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Koblenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

export default () =>
  <div>
    <Meta/>
    <Impressum/>
  </div>
