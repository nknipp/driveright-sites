'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import Courses from '../../../components/courses'
import RegisterBtn from '../../../components/register-btn'

import CoursesBgImg from '../images/hintergrund-unterseite-asf.jpg'

const Meta = () =>
  <Helmet>
    <title>Start Fahrschule Koblenz - Aufbauseminare für Fahranfänger!</title>
    <meta name="description" content="Die Start Fahrschule Koblenz ist in Koblenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Koblenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${CoursesBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Aufbauseminare für Fahranfänger!</h1>
              <p>Warst Du während der Probezeit zu schnell? Hast Dir einen gravierenden Verstoß oder zwei kleinere Verstöße zu schulden kommen lassen? Wir können Dir helfen! Bei uns findest Du regelmäßige Aufbauseminare um zukünftig sicherer im Straßenverkehr unterwegs zu sein.</p>
              <RegisterBtn title="Fragen zu unseren Aufbauseminaren?"/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Intro = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="row">
        <div className="col-xs-12 text-center">
          <h3>Hier findest Du den passenden Kurs in ...</h3>
        </div>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Intro/>
    <Courses tags="ASF"/>
  </div>
