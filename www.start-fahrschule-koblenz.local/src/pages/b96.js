'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import Courses from '../../../components/courses'
import RegisterBtn from '../../../components/register-btn'

import CoursesBgImg from '../images/hintergrund-unterseite-b96.jpg'

const Meta = () =>
  <Helmet>
    <title>Start Fahrschule Koblenz - Erweiterung auf B96!</title>
    <meta name="description" content="Die Start Fahrschule Koblenz ist in Koblenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Koblenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${CoursesBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Erweiterung auf B96!</h1>
              <p>Du interessierst Dich für die Fahrerschulung B96? Die START Fahrschule Koblenz bietet regelmäßige Kurse an. Durch die Erweiterung kannst Du zukünftig Anhänger ziehen die mehr als 750 kg in der Gesamtmasse betragen. In der Kombination (PKW + Anhänger) kannst Du dann eine Gesamtmasse von mehr als 3500 kg jedoch nicht mehr als 4250 kg führen.</p>
              <RegisterBtn title="Fragen zur Erweiterung auf B96?"/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Intro = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="row">
        <div className="col-xs-12 text-center">
          <h3>Hier findest Du den passenden Kurs in ...</h3>
        </div>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Intro/>
    <Courses tags="B96"/>
  </div>
