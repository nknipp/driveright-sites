'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import DrivingLicenceClasses from '../../../components/driving-licence-classes'

import RegisterBtn from '../../../components/register-btn'

import DrivingLicenceClassesBgImg from '../images/hintergrund-unterseite-ausbildungsklassen.jpg'

const Meta = () =>
  <Helmet>
    <title>Start Fahrschule Koblenz - Bei uns findet jeder die passende Ausbildungsklasse</title>
    <meta name="description" content="Die Start Fahrschule Koblenz ist in Koblenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Koblenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${DrivingLicenceClassesBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Bei uns findet jeder die passende Ausbildungsklasse!</h1>
              <p>Egal ob Du den Führerschein für das Mofa, Auto oder das Motorrad machen möchtest, wir bilden Dich sicher und zuverlässig aus. Wichtig ist nur, dass Du die entsprechenden Voraussetzungen für die Ausbildung erfüllst. Solltest Du noch Fragen haben, sind wir sehr gerne für Dich da.</p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Licences = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="works_content">
        <DrivingLicenceClasses
          classes={['mofa','am','a1','a2','a','b','b96','be','c1','c1e','c','ce','t','l']}
        />
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Licences/>
  </div>
