'use strict'

import React from 'react'

import Navbar from '../../../components/navbar-fixed-top'
import Footer from '../../../components/footer'
import WerKenntDenBesten from '../../../components/wer-kennt-den-besten'

import Logo from '../images/logo.png'

const Layout = ({ children }) =>
  <div>
    <div className="preloader">
      <p>Loading...</p>
    </div>

    <Navbar
      logo={Logo}
      navbarClass=''
      navs={[
        {
          title: 'Fahrschule',
          navs: [
            {
              href: '/filialen',
              title: 'Filialen'
            }, {
              href: '/team',
              title: 'Team'
            }, {
              href: '/fahrzeuge',
              title: 'Fahrzeuge'
            }]
        }, {
          title: 'Ausbildung',
          navs: [
            {
              href: '/ausbildungsklassen',
              title: 'Ausbildungsklassen'
            }, {
              href: '/kurse',
              title: 'Kurse'
            }, {
              href: '/bestanden',
              title: 'Bestanden'
            }, {
              href: '/online-anmeldung',
              title: 'Online-Anmeldung'
            }
          ]
        }, {
          href: '/vorteile',
          title: 'Vorteile'
        }, {
          href: '/asf',
          title: 'ASF'
        }, {
          href: '/b96',
          title: 'B96'
        }, {
          href: '/heldenmacher',
          title: 'Heldenmacher'
        }, {
          href: '/eltern',
          title: 'Eltern'
        }]}
    >
      <WerKenntDenBesten id='*be5mkU5t9jYvTKc33KGgktGB7e640tZ_HRIbGp3lZte0xMPHMrhESQlaWU_5NNoC1rHNOUIT6CmFI6w8jIDtcYorqZpdeN_06OSGVf4fqLbYiQeY1fEPgUGkS7ine_GqTGq6t1ITbZn0CxQYtPbklxJ8HszpdxPxjI0wKG2Kb1AIM43ZcV4R7DvKH7spte_QtdtIxz42WvQ'/>
    </Navbar>

    {children()}

    <Footer/>
  </div>

export default Layout
