'use strict'

import React from 'react'

class CampaignParticipants extends React.Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
    this.handleCheck = this.handleCheck.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.state = {
      solution: '',
      name: '',
      street: '',
      postcode: '',
      city: '',
      phone: '',
      emailAddress: '',
      acceptRequirements: false
    }
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleCheck(event) {
    const checked = this.state[event.target.name];
    this.setState({ [event.target.name]: !checked });
  }

  handleSubmit() {
    const serializedData = `{
      "query": "mutation { addCampaignParticipant(solution: \\"${this.state.solution}\\", name: \\"${this.state.name}\\", street: \\"${this.state.street}\\", postcode: \\"${this.state.postcode}\\", city: \\"${this.state.city}\\", phone: \\"${this.state.phone}\\", emailAddress: \\"${this.state.emailAddress}\\", acceptRequirements: ${this.state.acceptRequirements}) { success errors }}",
      "variables": null
    }`
    $.ajax({
      url: "https://api.drive-right.de/q",
      method: "POST",
      contentType: "application/json",
      data: serializedData,
      success: function(data) {
        if (!data.data.addCampaignParticipant.success) {
          let errorMessage = '<ul>';
          data.data.addCampaignParticipant.errors.forEach(function(err) {
            errorMessage += '<li>' + err + '</li>';
          });
          errorMessage += '</ul>';
          $('#contactRequestMessages').show().html(errorMessage);
        } else {
          $('#contactRequestMessages').hide().html('');
          $('#contactRequestForm').hide();
          $('#contactRequestSuccess').show();
        }
      }
    });
  }

  render() {
    return (
      <div className="contact-form wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
        <div id="requirementsDlg" className="modal fade" role="dialog">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h3 className="modal-title">Teilnahmebedingungen für das Gewinnspiel der <strong>Karnevals-Aktion</strong></h3>
              </div>
              <div className="modal-body">
                <h4>Veranstalter</h4>
                <p>
                  Der Veranstalter des Gewinnspiels ist die <strong>START Fahrschule Koblenz UG</strong> vertreten durch:<br/>
                  Alexandra Löhr-Müller<br/>
                  Moselring 1<br/>
                  56068 Koblenz<br/>
                  Tel.: 0261 / 29 63 47 82<br/>
                  Web: www.start-fahrschule-koblenz.de<br/>
                  E-Mail: startfahrschulekoblenz@gmail.com
                </p>

                <h4>Ablauf des Gewinnspiels</h4>
                <p>
                  Die Dauer des Gewinnspiels erstreckt sich vom 11.11.2018, 0:00 Uhr bis zum 5.3.2018, 23:59. Innerhalb dieses Zeitraums erhalten Nutzer online
                  die Möglichkeit, am Gewinnspiel teilzunehmen.
                </p>

                <h4>Teilnahme</h4>
                <p>
                  Um am Gewinnspiel teilzunehmen muss ein Orden des Karnevalsvereins Boomer Pitter käuflich erworben werden. Diese Orden enthalten eine
                  Visitenkarte mit einer Losnummer. Beim Ausfüllen und Absenden des angezeigten Teilnahmeformulars muss diese Losnummer eingegeben werden.
                  Die Teilnahme ist nur innerhalb des Teilnahmezeitraums möglich. Nach Teilnahmeschluss eingehende Einsendungen werden bei der Auslosung
                  nicht berücksichtigt.<br/>
                  Pro Teilnehmer nimmt nur eine übermittelte Anmeldung am Gewinnspiel teil. Es ist strengstens untersagt, mehrere E-Mail Adressen zur Erhöhung der
                  Gewinnchancen zu verwenden.<br/>
                  Die Teilnahme am Gewinnspiel ist kostenlos.
                </p>

                <h4>Teilnahmeberechtigte</h4>
                <p>
                  Zur Teilnahme musst du mindestens 16 Jahre alt sein und deinen Wohnsitz in Deutschland, der Schweiz oder in Österreich haben. Mit der Teilnahme
                  am Gewinnspiel stimmst du diesen Bedingungen zu.<br/>
                  Nicht teilnahmeberechtigt am Gewinnspiel sind alle an der Konzeption und Umsetzung des Gewinnspiels beteiligte Personen und Mitarbeiter des
                  Betreibers sowie ihre Familienmitglieder. Zudem behält sich der Betreiber vor, nach eigenem Ermessen Personen von der Teilnahme auszuschließen,
                  wenn berechtigte Gründe vorliegen, beispielsweise
                  <ol>
                    <li>bei Manipulationen im Zusammenhang mit Zugang zum oder Durchführung des Gewinnspiels</li>
                    <li>bei Verstößen gegen diese Teilnahmebedingungen</li>
                    <li>bei unlauterem Handeln</li>
                    <li>bei falschen oder irreführenden Angaben im Zusammenhang mit der Teilnahme an dem Gewinnspiel</li>
                  </ol>
                </p>

                <h4>Gewinn, Benachrichtigung und Übermittlung des Gewinns</h4>
                <p>
                  Der Gewinn sind START-Guthaben für die Führerscheinausbildung in unserem Hause im Gesamtwert von bis zu 1.274 Euro.<br/>
                  1. Preis: START-Guthaben im Wert von 1.274 Euro<br/>
                  2. Preis: START-Guthaben im Wert von 764 Euro<br/>
                  3. Preis: START-Guthaben im Wert von 514 Euro<br/>
                  4. Preis: START-Guthaben im Wert von 364 Euro<br/>
                  5. Preis: START-Guthaben im Wert von 295 Euro<br/>
                  6 - 10. Preis: START-Guthaben im Wert von 100 Euro<br/>
                  Der Gewinn ist übertragbar. Eine Barauszahlung oder eine Auszahlung des Gewinns in Sachwerten ist nicht möglich.<br/>
                  Die Ermittlung der Gewinner erfolgt nach Teilnahmeschluss im Rahmen einer auf dem Zufallsprinzip beruhenden Verlosung unter allen Teilnehmern.<br/>
                  Die Gewinner der Verlosung werden zeitnah per Telefon über den Gewinn informiert. Zudem werden die Gewinner über einen offiziellen Facebook-Post
                  auf unserer Facebook-Fanseite bekannt gegeben.<br/>
                  Meldet sich der Gewinner nach zweifacher Aufforderung innerhalb einer Frist von 3 Wochen nicht, kann der Gewinn auf einen anderen Teilnehmer
                  übertragen werden.
                </p>

                <h4>Beendigung des Gewinnspiels</h4>
                <p>
                  Der Veranstalter behält sich ausdrücklich vor, das Gewinnspiel ohne vorherige Ankündigung und ohne Mitteilung von Gründen zu beenden. Dies gilt
                  insbesondere für jegliche Gründe, die einen planmäßigen Ablauf des Gewinnspiels stören oder verhindern würden.
                </p>

                <h4>Datenschutz</h4>
                <p>
                  Für die Teilnahme am Gewinnspiel ist die Angabe von persönlichen Daten notwendig. Der Teilnehmer versichert, dass die von ihm gemachten Angaben
                  zur Person, insbesondere Vor-, Nachname und E-Mail Adresse wahrheitsgemäß und richtig sind.<br/>
                  Der Veranstalter weist darauf hin, dass sämtliche personenbezogenen Daten des Teilnehmers ohne Einverständnis weder an Dritte weitergegeben noch
                  diesen zur Nutzung überlassen werden.<br/>
                  Eine Ausnahme stellt das für die Durchführung des Gewinnspiels beauftragte Unternehmen driveright dar, welches die Daten zum Zwecke der Durchführung
                  des Gewinnspiels erheben und speichern muss.<br/>
                  Im Falle eines Gewinns, erklärt sich der Gewinner mit der Veröffentlichung seines Namens und Wohnorts in den vom Veranstalter genutzten Werbemedien
                  einverstanden. Dies schließt die Bekanntgabe des Gewinners auf der Webseite des Betreibers und seinen Social Media Plattformen mit ein.<br/>
                  Der Teilnehmer kann seine erklärte Einwilligung jederzeit widerrufen. Der Widerruf ist schriftlich an die im Impressumsbereich der Website angegebenen
                  Kontaktdaten des Veranstalters zu richten. Nach Widerruf der Einwilligung werden die erhobenen und gespeicherten personenbezogenen Daten des Teilnehmers
                  umgehend gelöscht.<br/>
                </p>

                <h4>Anwendbares Recht</h4>
                <p>
                  Fragen oder Beanstandungen im Zusammenhang mit dem Gewinnspiel sind an den Betreiber zu richten. Kontaktmöglichkeiten finden sich im Impressumsbereich
                  der Website www.start-fahrschule-koblenz.de.<br/>
                  Das Gewinnspiel des Betreibers unterliegt ausschließlich dem Recht der Bundesrepublik Deutschland. Der Rechtsweg ist ausgeschlossen.<br/>
                  Der Betreiber behält sich das Recht vor, diese Teilnahmebedingungen zu ändern.
                </p>

                <h4>Salvatorische Klausel</h4>
                <p>
                  Sollte eine Bestimmung dieser Teilnahmebedingungen ganz oder teilweise unwirksam sein oder werden, so wird dadurch die Gültigkeit dieser
                  Teilnahmebedingungen im Übrigen nicht berührt. Statt der unwirksamen Bestimmung gilt diejenige gesetzlich zulässige Regelung, die dem in
                  der unwirksamen Bestimmung zum Ausdruck gekommenen Sinn und Zweck wirtschaftlich am nächsten kommt. Entsprechendes gilt für den Fall des
                  Vorliegens einer Regelungslücke in diesen Teilnahmebedingungen.
                </p>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Akzeptieren</button>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-sm-12 col-md-8 col-md-offset-2">
            <form id="contactRequestForm" name="contactRequestForm" method="post" action="#">
              <div id="contactRequestMessages" style={{ display: "none" }} className="col-sm-10"></div>
              <div className="wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                <div className="form-group col-md-6 col-md-offset-3" style={{marginBottom: '50px'}}>
                  <input type="text" name="solution" className="form-control text-center" style={{fontSize: '38px', height: '80px'}} placeholder="Losnummer" required="required" onChange={this.handleChange}/>
                </div>
                <div className="form-group">
                  <input type="text" name="name" className="form-control" placeholder="Vor- und Zuname" required="required" onChange={this.handleChange}/>
                </div>
                <div className="form-group">
                  <input type="tel" name="phone" className="form-control" placeholder="Telefon" required="required" onChange={this.handleChange}/>
                </div>
                <div className="form-group">
                  <input type="text" name="street" className="form-control" placeholder="Strasse" onChange={this.handleChange}/>
                </div>
                <div className="row">
                  <div className="form-group col-md-3">
                    <input type="text" name="postcode" className="form-control" placeholder="Postleitzahl" onChange={this.handleChange}/>
                  </div>
                  <div className="form-group col-md-9">
                    <input type="text" name="city" className="form-control" placeholder="Wohnort" onChange={this.handleChange}/>
                  </div>
                </div>
                <div className="form-group">
                  <input type="email" name="emailAddress" className="form-control" placeholder="E-Mail Adresse" onChange={this.handleChange}/>
                </div>
                <div className="form-group interested-in">
                  <input type="checkbox" name="acceptRequirements" onClick={this.handleCheck} style={{ verticalAlign: "middle", width: "20px", height: "20px" }}/>
                  <label style={{ verticalAlign: "middle", marginLeft: 5 }}>Ich habe die <a data-toggle="modal" data-target="#requirementsDlg">Teilnahmebedingungen</a> gelesen und stimme diesen zu.</label>
                </div>
                <div className="form-group">
                  <button type="button" className="btn btn-primary btn-lg" onClick={this.handleSubmit}>Absenden</button>
                </div>
              </div>
            </form>
            <div id="contactRequestSuccess" className="contact-request-success" style={{ display: "none" }}>
              <h3 style={{ color: 'black' }}>Vielen Dank für Ihre Teilnahme.</h3>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default CampaignParticipants