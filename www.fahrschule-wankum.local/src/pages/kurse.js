'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import Courses from '../../../components/courses'
import RegisterBtn from '../../../components/register-btn'

import CoursesBgImg from '../images/hintergrund-unterseite-kurse.jpg'

const Meta = () =>
  <Helmet>
    <title>Fahrschule Wankum - Theorie innerhalb von nur 7 Werktagen!</title>
    <meta name="description" content="Die Fahrschule Wankum ist in Wegberg und Erkelenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wegberg, Führerscheinausbildung in Erkelenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${CoursesBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Theorie innerhalb von nur 7 Werktagen!</h1>
              <p>Du magst es schnell? Du magst es spannend? Und vor allem magst Du keine Langeweile? Dann solltest Du Dich zu unseren regelmäßíg stattfindenden Intensivkursen anmelden. Zum einen absolvierst Du innerhalb von nur 7 Werktagen den kompletten Stoff für die Theorievorbereitung und zum anderen lernst Du durch spannende Lernkonzepte ohne Langeweile!</p>
              <RegisterBtn title="Du hast weitere Fragen zu unseren Intensivkursen?"/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Intro = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="row">
        <div className="col-xs-12 text-center">
          <h3>Hier findest Du den passenden Kurs in ...</h3>
        </div>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Intro/>
    <Courses tags={['', null, 'Intensiv', 'Erste Hilfe', 'ASF', 'BKF', 'MPU']}/>
  </div>
