'use strict';

import React from 'react';
import {Helmet} from 'react-helmet';

import RegisterBtn from '../../../components/register-btn';

import BenefitsBgImg from '../images/hintergrund-unterseite-vorteile.jpg';
import BenefitsCar from '../images/deine-vorteile-grafik.png';

const Meta = () =>
  <Helmet>
    <title>Fahrschule Wankum - Deshalb sind wir für Dich die richtige Wahl!</title>
    <meta name="description" content="Die Fahrschule Wankum ist in Wegberg und Erkelenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wegberg, Führerscheinausbildung in Erkelenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>;

const Header = () =>
  <section>
    <div className="welcome-header" style={{backgroundImage: `url(${BenefitsBgImg})`}}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Deshalb sind wir für Dich die richtige Wahl!</h1>
              <p>Wir sind stolz darauf Dir viele Vorteile anbieten zu können. Neben den neusten Trends, Fahrfreude
                während deiner Führerscheinausbildung und digitale Lernmöglichkeiten findest Du zudem diese Highlights
                bei uns:</p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>;

const Content = () =>
  <section className="benefits section-space-padding" style={{backgroundColor: '#f9de2b'}}>
    <div className="container">
      <div className="row">
        <div className="benefits-container">
          <div className="col-md-4 col-sm-12 col-xs-12">
            <div className="text-right wow fadeInLeft" data-wow-duration="2s">
              <div className="benefits-item wow fadeInUp" data-wow-duration="1.5s">
                <div className="benefits-circle">
                  <i className="fa fa-rocket"></i>
                </div>
                <div className="benefits-content">
                  <h4>Intensivkurse</h4>
                  <p>Du hast keine Lust auf gezogenen Theorieunterricht? Keine Lust auf eine
                    „never-ending-Führerscheinstory“? Wir lieben KOMPAKT! Unterricht innerhalb 7 Werktagen,
                    Führerscheinausbildung innerhalb 14 Tage oder ein Ausbildungsplan individuell auf Dich zugeschnitten
                    – das zeichnet uns aus!</p>
                </div>
              </div>

              <div className="benefits-item wow fadeInUp" data-wow-duration="1.5s">
                <div className="benefits-circle">
                  <i className="fa fa-pencil-square-o"></i>
                </div>
                <div className="benefits-content">
                  <h4>Theorie Vortest</h4>
                  <p>Keine Panik vor der Theorieprüfung! Wir starten mit Dir einen Vortest und geben Dir erst das
                    Startzeichen wenn es wirklich Sinn macht!</p>
                </div>
              </div>
            </div>
          </div>

          <div className="col-md-4 col-sm-12 col-xs-12">
            <div className="text-center wow fadeInDown" data-wow-duration="2s">
              <img src={BenefitsCar} alt=""/>
            </div>
          </div>

          <div className="col-md-4 col-sm-12 col-xs-12">
            <div className="text-left wow fadeInRight" data-wow-duration="2s">
              <div className="benefits-item wow fadeInUp" data-wow-duration="1.5s">
                <div className="benefits-circle">
                  <i className="fa fa-desktop"></i>
                </div>
                <div className="benefits-content">
                  <h4>Simulator</h4>
                  <p>Durch den Fahrsimulator lernst du alles Notwendige vorab und kannst anschließend entspannt die
                    erste Fahrstunde beginnen.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </section>;

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Content/>
  </div>
