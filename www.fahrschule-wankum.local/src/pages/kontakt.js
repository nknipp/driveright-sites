'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import ContactRequest from '../../../components/contact-request'
import Stores from '../../../components/stores'

import ContactBgImg from '../images/hintergrund-unterseite-kontakt.jpg'

const Meta = () =>
  <Helmet>
    <title>Fahrschule Wankum - Wir warten auf Dich!</title>
    <meta name="description" content="Die Fahrschule Wankum ist in Wegberg und Erkelenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wegberg, Führerscheinausbildung in Erkelenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

export default () =>
  <div>
    <Meta/>
    <div className="welcome-header" style={{ backgroundImage: `url(${ContactBgImg})` }}>
      <div className="container heading-text" style={{ color: '#fff' }}>
        <div className="row">
          <h1>Wir warten auf Dich!</h1>
          <p>Keine Sorge und vor allem keine Angst! Der Führerschein ist oftmals die erste große Entscheidung im Leben. Ganz klar das dadurch auch Fragen oder Unsicherheiten auftreten. Aus diesem Grund steht Dir unser Team sehr gerne mit Rat und Tat zur Seite.</p>
          <p>Schreibe uns bequem über unser Kontaktformular, rufe uns an oder besuche uns persönlich während unseren Öffnungszeiten in unseren Filialen. Wir helfen Dir sehr gerne und vor allem unverbindlich weiter!</p>
        </div>
        <div className="row" style={{ paddingTop: '40px', paddingBottom: '40px' }}>
          <div className="col-sm-12 col-md-6">
            <ContactRequest/>
          </div>
          <div className="col-sm-12 col-md-5 col-md-offset-1">
            <Stores mode='inline'/>
          </div>
        </div>
      </div>
    </div>
  </div>
