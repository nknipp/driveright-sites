'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import Courses from '../../../components/courses'
import RegisterBtn from '../../../components/register-btn'

import CoursesBgImg from '../images/hintergrund-unterseite-b96.jpg'

const Meta = () =>
  <Helmet>
    <title>Fahrschule Wankum - Fahrerschulung B96!</title>
    <meta name="description" content="Die Fahrschule Wankum ist in Wegberg und Erkelenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wegberg, Führerscheinausbildung in Erkelenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${CoursesBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Fahrerschulung B96!</h1>
              <p>Dir reicht deine bisherige zugelassene Gesamtmasse von 750 kg in Bezug auf Anhänger nicht mehr aus? Du möchtest in der Kombination zukünftig insgesamt 4,25 Tonnen ziehen dürfen? Dann solltest Du Dich zu unseren Kursen regelmäßigen Kursen anmelden!</p>
              <RegisterBtn title="Du hast Fragen zu der Fahrerschulung B96?"/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Intro = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="row">
        <div className="col-xs-12 text-center">
          <h3>Hier findest Du den passenden Kurs in ...</h3>
        </div>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Intro/>
    <Courses tags="B96"/>
  </div>
