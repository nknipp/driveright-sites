'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import DrivingLicenceClasses from '../../../components/driving-licence-classes'
import RegisterBtn from '../../../components/register-btn'

import DrivingLicenceClassesBgImg from '../images/hintergrund-unterseite-ausbildungsklassen.jpg'

const Meta = () =>
  <Helmet>
    <title>Fahrschule Wankum - Für alle die passende Klasse dabei!</title>
    <meta name="description" content="Die Fahrschule Wankum ist in Wegberg und Erkelenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wegberg, Führerscheinausbildung in Erkelenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${DrivingLicenceClassesBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Für alle die passende Klasse dabei!</h1>
              <p>Unser Team bietet Dir eine große Vielfalt an Ausbildungsklassen an. Hier kannst Du Dich über die einzelnen Ausbildungsmöglichkeiten informieren. Für weitere Details und Informationen darfst Du Dich gerne mit uns in Verbindung setzen.</p>
              <RegisterBtn title="Was ist die passende Ausbildungsklasse für mich?"/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Licences = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="works_content">
        <DrivingLicenceClasses
          classes={['a','a1','a2','am','mofa','b','be','b96','c1','c1e','c','ce','d1','d1e','d','de','l','t']}
          imageSet="set2"
        />
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Licences/>
  </div>
