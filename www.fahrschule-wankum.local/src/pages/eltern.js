'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import RegisterBtn from '../../../components/register-btn'

import ParentsBgImg from '../images/hintergrund-unterseite-eltern.jpg'

const Meta = () =>
  <Helmet>
    <title>Fahrschule Wankum - Mama, ich werde bald 18 und bin erwachsen...</title>
    <meta name="description" content="Die Fahrschule Wankum ist in Wegberg und Erkelenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wegberg, Führerscheinausbildung in Erkelenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${ParentsBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Mama, ich werde bald 18 und bin erwachsen...</h1>
              <p>...Wenn ich 18 bin habt ihr mir gar nichts mehr zu sagen! - Welche Eltern kennen diese Aussagen nicht? Aber keine Sorge, Sie stehen mit diesem Problem nicht alleine da. Denn ihr Kind wird erwachsen!</p>
              <RegisterBtn title="Wir beantworten alle Fragen!"/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Content = () =>
  <section>
    <div className="container">
      <div className="row content">
        <p>Logischerweise wird durch dieses Alter auch der Führerschein zu einem sehr wichtigen und wahrscheinlich auch permanenten Thema. Für ihr Kind bedeutet ein Führerschein vor allem eins: <strong>Freiheit und Unabhängigkeit</strong>!</p>
        <p>Für Sie als Eltern bedeutet der Führerschein noch ein bisschen mehr <strong>loslassen</strong>.</p>
        <p>Aber wenn Sie ihr Kind schon ein Stück weiter in die Freiheit entlassen, dann doch mit einem guten Gefühl! Dieses Gefühl möchten und können wir Ihnen geben. Denn wir garantieren Ihnen, dass wir ihr Kind sicher und zuverlässig ausbilden werden. Um sich davon selbst zu überzeugen, laden wir Sie - und gerne auch ihr Kind - dazu ein, unsere Fahrschule zu besichtigen und mit den entsprechenden Ausbildern persönlich zu sprechen.</p>
        <p><strong>Wir freuen uns auf Sie und versichern Ihnen, dass ihr Kind auch mit Führerschein noch genau so oft “Mama” oder “Papa” sagen wird, wie ohne Führerschein!</strong></p>
        <RegisterBtn title="Wir beantworten alle Fragen!"/>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Content/>
  </div>
