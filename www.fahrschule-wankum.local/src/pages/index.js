'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import RegisterBtn from '../../../components/register-btn'
import ParallaxSection from '../../../components/parallax-section'
import Stores from '../../../components/stores'

import WelcomeBgImg from '../images/hintergrund-willkommen.jpg'
import WelcomeImg from '../images/willkommen.png'
import TheoryBgImg from '../images/hintergrund-theorie.jpg'
import BenefitsCar from '../images/deine-vorteile-grafik.png'

const Meta = () =>
  <Helmet>
    <title>Fahrschule Wankum - Moderne Führerscheinausbildung mit Freude, Spaß und Zuverlässigkeit</title>
    <meta name="description" content="Die Fahrschule Wankum ist in Wegberg und Erkelenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wegberg, Führerscheinausbildung in Erkelenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Welcome = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${WelcomeBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-5 col-xs-12">
            <div className="center-content wow fadeInLeft" data-wow-duration="1s">
              <img src={WelcomeImg} alt="Wollkommen" className="img-responsive" />
            </div>
          </div>
          <div className="col-sm-7 col-xs-12 heading-text" style={{ paddingBottom: '40px' }}>
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Hier bist Du richtig!</h1>
              <p>Du hast vor in der nächsten Zeit deinen Führerschein zu machen und suchst noch eine innovative Fahrschule? Dann bist Du bei uns genau richtig! Als Fahrschule in Wegberg und Erkelenz sind wir die sichere und zuverlässige Lösung für deine zukünftige Unabhängigkeit!</p>
              <RegisterBtn title="Wir beraten Dich gerne!"/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Benefits = () =>
  <section className="benefits" style={{ backgroundColor: '#f9de2b' }}>
    <div className="container" style={{ padding: '140px 0'}}>
      <div className="row">
        <div className="benefits-container">
          <div className="text-center">
            <h2 style={{ paddingBottom: '40px' }}>Viele Gründe, um Deinen Führerschein bei uns zu machen:</h2>
          </div>
          <div className="col-md-4 col-sm-12 col-xs-12">
            <div className="text-right wow fadeInLeft" data-wow-duration="2s">
              <div className="benefits-item wow fadeInUp" data-wow-duration="1.5s">
                <div className="benefits-circle">
                  <i className="fa fa-rocket"></i>
                </div>
                <div className="benefits-content">
                  <h4>Intensivkurse</h4>
                  <p>Du hast keine Lust auf gezogenen Theorieunterricht? Keine Lust auf eine „never-ending-Führerscheinstory“? Wir lieben KOMPAKT! Unterricht innerhalb 7 Werktagen, Führerscheinausbildung innerhalb 14 Tage oder ein Ausbildungsplan individuell auf Dich zugeschnitten – das zeichnet uns aus!</p>
                </div>
              </div>

              <div className="benefits-item wow fadeInUp" data-wow-duration="1.5s">
                <div className="benefits-circle">
                  <i className="fa fa-pencil-square-o"></i>
                </div>
                <div className="benefits-content">
                  <h4>Theorie Vortest</h4>
                  <p>Keine Panik vor der Theorieprüfung! Wir starten mit Dir einen Vortest und geben Dir erst das Startzeichen wenn es wirklich Sinn macht!</p>
                </div>
              </div>
            </div>
          </div>

          <div className="col-md-4 col-sm-12 col-xs-12">
            <div className="text-center wow fadeInDown" data-wow-duration="2s">
              <img src={BenefitsCar} alt="" />
            </div>
          </div>

          <div className="col-md-4 col-sm-12 col-xs-12">
            <div className="text-left wow fadeInRight" data-wow-duration="2s">
              <div className="benefits-item wow fadeInUp" data-wow-duration="1.5s">
                <div className="benefits-circle">
                  <i className="fa fa-desktop"></i>
                </div>
                <div className="benefits-content">
                  <h4>Simulator</h4>
                  <p>Durch den Fahrsimulator lernst du alles Notwendige vorab und kannst anschließend entspannt die erste Fahrstunde beginnen.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </section>

const Theory = () =>
  <ParallaxSection backgroundImage={TheoryBgImg}>
    <div className="text-center" style={{ color: '#000' }}>
      <h2>Einfach Spaß haben! Langeweile? Nicht bei uns!</h2>
      <p>Du kannst Dich entscheiden unseren Theorieunterricht in den verschiedenen Filialen zu besuchen und selber zeitlich zu planen oder an einem Intensivkurs teilzunehmen und den kompletten Pflichtstoff innerhalb von 7 Werktagen zu absolvieren.</p>
      <p>Egal wie Du Dich entscheidest: Durch unsere spannenden Lernkonzepte wird der Theorieunterricht zum Highlight!</p>
      <RegisterBtn/>
    </div>
  </ParallaxSection>

const Offices = () =>
  <section>
    <div className="container-fluid" style={{ backgroundColor: '#0b33c2', padding: '140px 0' }}>
      <div className="offices-bg">
        <div className="stores-heading-text text-center">
          <h2>Wir freuen uns auf deinen Besuch!</h2>
          <p>Persönlichkeit ist ein wichtiger Punkt während einer Führerscheinausbildung. Nicht nur deshalb sind wir 4x die Woche für Dich erreichbar:</p>
        </div>
        <Stores mode="block"/>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Welcome/>
    <Benefits/>
    <Theory/>
    <Offices/>
  </div>
