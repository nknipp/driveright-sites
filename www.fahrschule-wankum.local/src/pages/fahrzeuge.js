'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import Cars from '../../../components/cars'
import RegisterBtn from '../../../components/register-btn'

import CarsBgImg from '../images/hintergrund-unterseite-fahrzeuge.jpg'

const Meta = () =>
  <Helmet>
    <title>Fahrschule Wankum - Unsere Flotte!</title>
    <meta name="description" content="Die Fahrschule Wankum ist in Wegberg und Erkelenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wegberg, Führerscheinausbildung in Erkelenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${CarsBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Unsere Flotte!</h1>
              <p>Zu jeder Führerscheinausbildung - egal in welcher Klasse - gehört ein Ausbildungsfahrzeug. Diese möchten wir euch hier vorstellen. Selbstverständlich werden unsere Ausbildungsfahrzeuge regelmäßig zur Inspektion geschickt und entsprechen dem heutigen modernen Standard!</p>
              <RegisterBtn title="Du hast Fragen zu unseren Fahrzeugen?"/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Content = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="row">
        <Cars/>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Content/>
  </div>

