'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import RegisterBtn from '../../../components/register-btn'
import PassedGallery from '../../../components/passed-gallery'

import PassedBgImg from '../images/hintergrund-unterseite-bestanden.jpg'

const Meta = () =>
  <Helmet>
    <title>Fahrschule Wankum - Wir wünschen gute Fahrt!</title>
    <meta name="description" content="Die Fahrschule Wankum ist in Wegberg und Erkelenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wegberg, Führerscheinausbildung in Erkelenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${PassedBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Wir wünschen gute Fahrt!</h1>
              <p>Wir gratulieren zur erfolgreichen Prüfung! Nie wieder Bus! Oder sagen wir es mal so: Nie wieder auf den Bus angewiesen sein! Danke für die tolle Erfahrungen und gute Fahrt in der Zukunft</p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Passed = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="row">
        <PassedGallery renderMode="carousel" wrapperClassName="about-me-text"/>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Passed/>
  </div>
