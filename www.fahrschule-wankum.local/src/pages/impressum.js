'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import Impressum from '../../../components/impressum'

const Meta = () =>
  <Helmet>
    <title>Fahrschule Wankum - Impressum</title>
    <meta name="description" content="Die Fahrschule Wankum ist in Wegberg und Erkelenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wegberg, Führerscheinausbildung in Erkelenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

export default () =>
  <div>
    <Meta/>
    <Impressum/>
  </div>
