'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import Team from '../../../components/team'
import RegisterBtn from '../../../components/register-btn'

import TeamBgImg from '../images/hintergrund-unterseite-team.jpg'

const Meta = () =>
  <Helmet>
    <title>Fahrschule Wankum - Wir sind für Dich da!</title>
    <meta name="description" content="Die Fahrschule Wankum ist in Wegberg und Erkelenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wegberg, Führerscheinausbildung in Erkelenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${TeamBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Wir sind für Dich da!</h1>
                <p>Wir sind erst dann zufrieden, wenn Du mit Freude im Gesicht Deinen Führerschein übergeben bekommst. Aus diesem Grund steht Dir ein professionelles Team zur Seite um sowohl alle Fragen als auch alle Aufgaben schon im Vorfeld zu besprechen bzw. zu klären.</p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Content = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="row">
        <div>
          <Team cssClass="box-border" style={{ paddingBottom: '100px' }} />
        </div>
      </div>
      <div className="col-md-12 text-center">
        <RegisterBtn/>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Content/>
  </div>
