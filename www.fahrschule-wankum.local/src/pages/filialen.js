'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import Carousel from '../../../components/carousel'
import RegisterBtn from '../../../components/register-btn'

import OfficesBgImg from '../images/hintergrund-unterseite-filialen.jpg'

const Meta = () =>
  <Helmet>
    <title>Fahrschule Wankum - Wir sind Wegberg! Wir sind Erkelenz!</title>
    <meta name="description" content="Die Fahrschule Wankum ist in Wegberg und Erkelenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wegberg, Führerscheinausbildung in Erkelenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${OfficesBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Wir sind Wegberg! Wir sind Erkelenz!</h1>
              <p></p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


const CarouselSection = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="row" style={{ paddingBottom: '50px' }}>
        <div className="col-md-6 col-md-offset-3 col-xs-12">
          <div className="box-border">
            <Carousel>
              <div className="item">
                Bild 1
              </div>
              <div className="item">
                Bild 2
              </div>
              <div className="item">
                Bild 3
              </div>
            </Carousel>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-6 col-md-offset-3 col-xs-12">
          <p>Bei uns finden regelmäßige Intensivkurse statt, das bedeutet, dass Du innerhalb von sieben Werktagen Deinen kompletten Theoriestoff absolvieren kannst und fit für die Theorieprüfung und die praktische Ausbildung bist.</p>
          <p>Solltest Du Interesse haben an dem Kurs teilzunehmen, freuen wir uns selbstverständlich auf Deine Anfrage. In Kürze findest Du zudem die Möglichkeit unsere Kurse direkt über unsere Webseite zu buchen!</p>
        </div>
      </div>
      <div className="row text-center">
        <RegisterBtn/>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <CarouselSection/>
  </div>
