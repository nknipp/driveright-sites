'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import RegisterBtn from '../../../components/register-btn'
import ParallaxSection from '../../../components/parallax-section'
import Carousel from '../../../components/carousel'

import WelcomeBgImg from '../images/hintergrund-willkommen.jpg'
import WelcomeImg from '../images/willkommen.png'
import TheoryBgImg from '../images/hintergrund-theorie.jpg'
import AboutUsImg from '../images/das-sind-wir.png'
import StatisticImg from '../images/hintergrund-statistiken.jpg'
import ThanksImg from '../images/danke.png'

const Meta = () =>
  <Helmet>
    <title>Start Fahrschule Koblenz - Moderne Führerscheinausbildung mit Freude, Spaß und Zuverlässigkeit</title>
    <meta name="description" content="Die Start Fahrschule Koblenz ist in Koblenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Koblenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Welcome = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${WelcomeBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-5 col-xs-12">
            <div className="center-content wow fadeInLeft" data-wow-duration="1s">
              <img src={WelcomeImg} alt="Wollkommen" className="img-responsive" />
            </div>
          </div>
          <div className="col-sm-7 col-xs-12 heading-text" style={{ paddingBottom: '40px' }}>
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Bock auf Führerschein?</h1>
              <p>Dann hast Du Glück, dass Du uns gefunden hast! Wir sind der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung in Koblenz geht. Wenn Du auf der Suche nach einer sicheren und zuverlässigen Fahrschule bist, bei der ebenfalls der Spaß ganz oben auf der Liste steht, solltest Du uns unverbindlich ansprechen. Dann könnte das zwischen uns definitiv passen!</p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Benefits = () =>
  <section className="benefits">
    <div className="container">
      <div className="row">
        <div className="benefits-container">
          <div className="text-center">
            <h2>Viele Gründe, um Deinen Führerschein bei uns zu machen:</h2>
          </div>
          <div className="col-md-6 col-sm-12 col-xs-12">
            <div className="benefits-item wow fadeInUp" data-wow-duration=".5s">
              <div className="benefits-circle">
                <i className="fa fa-magic"></i>
              </div>
              <div className="benefits-content">
                <h4>Mentalcoaching</h4>
                <p>FAHRANGST war gestern! Mit uns fährst Du ANGSTfrei!</p>
                <p>Ob Angst vor der Theorie oder der Praxis (Prüfungsangst), ob Trauma nach einem Unfall oder “plötzlich scheinbar ohne Grund da” - wir helfen weiter!</p>
                <p>Unser Fahrtrainer Marco Willems ist nicht “Fahrlehrer” sondern auch Hypnose-Coach und Mentaltrainer und führt diese “Anti-Angst-Trainings” druch.</p>              </div>
            </div>
          </div>
          <div className="col-md-6 col-sm-12 col-xs-12">
            <div className="benefits-item wow fadeInUp" data-wow-duration=".8s">
              <div className="benefits-circle">
                <i className="fa fa-desktop"></i>
              </div>
              <div className="benefits-content">
                <h4>Easy-START-Fahrsimulator</h4>
                <p>Durch den Fahrsimulator hast Du die Möglichkeit, schon vor Deiner ersten Fahrstunde im Fahrschulwagen die ersten “Schritte” des Autofahrenlernens zu machen - ganz entspannt, in Ruhe und Du bestimmst das Tempo! Das gibt Dir für Deine erste Fahrstunde Sicherheit und vor allem Gelassenheit!</p>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-sm-12 col-xs-12">
            <div className="benefits-item wow fadeInUp" data-wow-duration="1.2">
              <div className="benefits-circle">
                <i className="fa fa-video-camera"></i>
              </div>
              <div className="benefits-content">
                <h4>Praxis-App</h4>
                <p>Wir haben unser Prüfgebiet für Dich verfilmen lassen und die kniffligsten Stellen in einer App zusammengestellt. So kannst Du ganz bequem von zu Hause aus Dich auf diese Situationen einstellen!</p>              </div>
            </div>
          </div>
          <div className="col-md-6 col-sm-12 col-xs-12">
            <div className="benefits-item wow fadeInUp" data-wow-duration="1.5s">
              <div className="benefits-circle">
                <i className="fa fa-rocket"></i>
              </div>
              <div className="benefits-content">
                <h4>Intensivkurse</h4>
                <p>Du hast keine Lust auf gezogenen Theorieunterricht? Keine Lust auf eine „never-ending-Führerscheinstory“? Wir lieben KOMPAKT! Unterricht innerhalb 7 Werktagen, Führerscheinausbildung innerhalb 14 Tage oder ein Ausbildungsplan individuell auf Dich zugeschnitten – das zeichnet uns aus!</p>              </div>
            </div>
          </div>
          <div className="col-md-6 col-sm-12 col-xs-12">
            <div className="benefits-item wow fadeInUp" data-wow-duration="1.9s">
              <div className="benefits-circle">
                <i className="fa fa-expeditedssl"></i>
              </div>
              <div className="benefits-content">
                <h4>Theorie Garantie</h4>
                <p>Solltest du trotz unserer speziellen Vorprüfung die Theorieprüfung nicht bestehen, übernehmen wir den Fahrschulbetrag für die nächste Theorieprüfung!</p>              </div>
            </div>
          </div>
          <div className="col-md-6 col-sm-12 col-xs-12">
            <div className="benefits-item wow fadeInUp" data-wow-duration="3.2s">
              <div className="benefits-circle">
                <i className="fa fa-arrows-alt"></i>
              </div>
              <div className="benefits-content">
                <h4>Freie Fahrlehrerwahl</h4>
                <p>Weil Du uns wichtig bist: Bei uns kannst Du Dir deinen Fahrlehrer aussuchen denn nur in einer guten Atmosphäre lässt es sich gut lernen!</p>              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Theory = () =>
  <ParallaxSection backgroundImage={TheoryBgImg}>
    <div className="text-center">
      <h2>Theorieunterricht geht auch cool!</h2>
      <p>Die Zeiten in denen man sich zum Theorieunterricht quälen muss sind vorbei! Wir begeistern Dich mit unseren spannenden Lernkonzepten und machen jede Theorieeinheit zu einem unvergesslichen Erlebnis!</p>
      <RegisterBtn/>
    </div>
  </ParallaxSection>

const AboutUs = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="row">
        <div className="col-sm-12">
          <div className="section-title">
            <h2>Das sind wir...</h2>
            <div className="divider dark"></div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-6" style={{ marginBottom: '50px' }}>
          <div className="about-me-text">
            <img src={AboutUsImg} className="img-responsive" />
          </div>
        </div>
        <div className="col-md-6">
          <div className="about-me-text" style={{ marginBottom: '50px'}}>
            <div className="text-center">
              Eigentlich gibt es nicht viel über uns zu erzählen, wir sind eine innovative und zielorientierte Fahrschule direkt in Koblenz. Neben den Dingen die Du bei jeder anderen Fahrschule auch findest, bieten wir Dir vor allem eine ehrliche Führerscheinausbildung und sind generell „ein bisschen anders“.
            </div>
          </div>
          <div className="about-me-text">
            <div className="text-center">
              Wir sehen Dich nicht nur als Fahrschüler sondern als ein Freund mit dem wir eine überragende Zeit verbringen dürfen und glücklich darüber sind, dass wir mit Dir Dein Lebensereignis „Führerschein“ erleben dürfen. Solltest Du Dich für eine Führerscheinausbildung interessieren, Fragen haben oder uns gerne persönlich kennenlernen möchtest, freuen wir uns auf Dich!
            </div>
          </div>
        </div>
      </div>
      <div className="row text-center">
        <RegisterBtn/>
      </div>
    </div>
  </section>

const Statistics = () =>
  <section className="section-space-padding" style={{ backgroundImage: `url(${StatisticImg})` }}>
    <div className="container">
      <div className="row text-center" style={{ color: '#fff', paddingBottom: '20px' }}>
        <h2>In den letzten Jahren...</h2>
      </div>
      <div className="row">
        <div className="col-md-3 col-sm-6 col-xs-12">
          <div className="statistics text-center">
            <div className="statistics-icon"><i className="fa fa-road"></i>
            </div>
            <div className="statistics-content">
              <h4>+ 500.000 km</h4>
              <p>für unsere Fahrschüler unterwegs!</p>
            </div>
          </div>
        </div>
        <div className="col-md-3 col-sm-6 col-xs-12">
          <div className="statistics text-center">
            <div className="statistics-icon"><i className="fa fa-flag-checkered"></i>
            </div>
            <div className="statistics-content">
              <h4>+ 2.500 Fahrschüler</h4>
              <p>zum Führerschein verholfen!</p>
            </div>
          </div>
        </div>
        <div className="col-md-3 col-sm-6 col-xs-12">
          <div className="statistics text-center">
            <div className="statistics-icon"><i className="fa fa-car"></i>
            </div>
            <div className="statistics-content">
              <h4>+ 3</h4>
              <p>moderne Ausbildungsfahrzeuge!</p>
            </div>
          </div>
        </div>
        <div className="col-md-3 col-sm-6 col-xs-12">
          <div className="statistics text-center">
            <div className="statistics-icon"><i className="fa fa-bullhorn"></i>
            </div>
            <div className="statistics-content">
              <h4>+ 6</h4>
              <p>köpfiges Team für deinen Erfolg!</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Reviews = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="row">
        <div className="col-xs-12 text-center">
          <h2>Das sagen unsere Fahrschüler!</h2>
          <p>Führerschein bestehen darf nicht mit Führerschein bestehen verwechselt werden. Der Unterschied ist eine geile Zeit!</p>
        </div>
        <div className="col-xs-12" style={{ marginTop: '50px' }}>
          <div className="about-me-text">
            <Carousel>
              <div className="item">
                <div className="col-xs-12 col-md-2 col-md-offset-1">
                  <img src={ThanksImg} style={{ width: '182px' }} />
                </div>
                <div className="col-xs-12 col-md-6 col-md-offset-1 text-center">
                  <h2>Hanna Dötsch aus Koblenz</h2>
                  <p><strong>Super Ausbildung!</strong> Vielen Dank an Marco! :D Er hat mich mit viel Geduld gut auf die Prüfung vorbereitet, mir mit Tipps durch die Fahrstunden geholfen und dafür gesorgt, dass diese immer Spaß machen!</p>
                </div>
              </div>
              <div className="item">
                <div className="col-xs-12  col-md-2 col-md-offset-1">
                  <img src={ThanksImg} style={{ width: '182px' }} />
                </div>
                <div className="col-xs-12 col-md-6 col-md-offset-1 text-center">
                  <h2>Helena aus Koblenz</h2>
                  <p><strong>Alles super gelaufen!</strong> Es passt alles! Mit dem Fahrlehrer konnte man individuelle Absprachen für die Fahrstunden treffen; er war sehr engagiert!</p>
                </div>
              </div>
              <div className="item">
                <div className="col-xs-12  col-md-2 col-md-offset-1">
                  <img src={ThanksImg} style={{ width: '182px' }} />
                </div>
                <div className="col-xs-12 col-md-6 col-md-offset-1 text-center">
                  <h2>Benedikt aus Koblenz</h2>
                  <p><strong>Fahrstunden waren einfach super lustig und entspannt</strong>. Nur zu empfehlen! Die Fahrschule, der Intensivkurs und das Team!</p>
                </div>
              </div>
            </Carousel>
          </div>
        </div>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Welcome/>
    <Benefits/>
    <Theory/>
    <AboutUs/>
    <Statistics/>
    <Reviews/>
  </div>
