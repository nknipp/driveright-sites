'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import ContactRequest from '../../../components/contact-request'
import Stores from '../../../components/stores'

import ContactBgImg from '../images/hintergrund-unterseite-kontakt.jpg'

const Meta = () =>
  <Helmet>
    <title>Start Fahrschule Koblenz - Jetzt wird es ernst! Komm vorbei, ruf uns an oder schreibe uns!</title>
    <meta name="description" content="Die Start Fahrschule Koblenz ist in Koblenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Koblenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

export default () =>
  <div>
    <Meta/>
    <div className="welcome-header" style={{ backgroundImage: `url(${ContactBgImg})` }}>
      <div className="container heading-text" style={{ color: '#fff' }}>
        <div className="row">
          <h1>Jetzt wird es ernst!</h1>
          <p>Wir freuen uns darüber, dass Du uns kontaktieren möchtest. Wir sind während unserer Öffnungszeiten selbstverständlich auch persönlich für Deine Fragen da. Gerne kannst Du uns aber auch über unsere Webseite eine Anfrage senden oder ganz unkompliziert anrufen.</p>
        </div>
        <div className="row" style={{ paddingTop: '40px', paddingBottom: '40px' }}>
          <div className="col-sm-12 col-md-6">
            <ContactRequest/>
          </div>
          <div className="col-sm-12 col-md-5 col-md-offset-1">
            <Stores mode='inline'/>
          </div>
        </div>
      </div>
    </div>
  </div>
