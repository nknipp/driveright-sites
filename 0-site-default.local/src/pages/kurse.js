'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import Courses from '../../../components/courses'
import RegisterBtn from '../../../components/register-btn'

import CoursesBgImg from '../images/hintergrund-unterseite-kurse.jpg'

const Meta = () =>
  <Helmet>
    <title>Start Fahrschule Koblenz - Bock auf Kompakt? Hier findest Du unsere Kurse.</title>
    <meta name="description" content="Die Start Fahrschule Koblenz ist in Koblenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Koblenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${CoursesBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Bock auf Kompakt?</h1>
              <p>Hier findest Du eine Übersicht unserer Kurse! Jeden Monat hast Du die Möglichkeit Deinen Theorieunterricht innerhalb von nur 7 Werktagen zu erledigen. Aber nicht nur schnell, sondern auch effektiv. Durch spannende Gruppen- und Lernkonzepte wird Dir die Zeit sogar sehr viel Freude bereiten!</p>
              <p>Du interessierst Dich für einen unserer Kurse? Dann nehme Kontakt mit uns auf!</p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Intro = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="row">
        <div className="col-xs-12 text-center">
          <h3>Hier findest Du den passenden Kurs in ...</h3>
        </div>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Intro/>
    <Courses/>
  </div>
