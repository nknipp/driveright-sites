'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import RegisterBtn from '../../../components/register-btn'
import PassedGallery from '../../../components/passed-gallery'

import PassedBgImg from '../images/hintergrund-unterseite-bestanden.jpg'

const Meta = () =>
  <Helmet>
    <title>Start Fahrschule Koblenz - Yeah! Geschafft!</title>
    <meta name="description" content="Die Start Fahrschule Koblenz ist in Koblenz der perfekte Ansprechpartner, wenn es um Deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Koblenz, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div className="welcome-header" style={{ backgroundImage: `url(${PassedBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Yeah! Geschafft!</h1>
              <p>Nie wieder Bus? Nie wieder Fahrrad? - Naja, vielleicht nicht unbedingt. Aber ihr seit nicht mehr davon abhängig. Denn eure Unabhängigkeit hat begonnen! Wir gratulieren unseren Fahrschüler die nicht nur uns sondern auch den Prüfer überzeugen konnten.</p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Passed = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="row">
        <PassedGallery renderMode="carousel" wrapperClassName="about-me-text"/>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Passed/>
  </div>
