'use strict'

import React from 'react'

import Navbar from '../../../components/navbar-fixed-top'
import Footer from '../../../components/footer'
import SocialPlugin from '../../../components/social-plugins'

import Logo from '../images/logo.png'

const Layout = ({children}) =>
  <div>
    <div className="preloader">
      <p>Loading...</p>
    </div>

    <Navbar
      logo={Logo}
      navbarClass=''
      navs={[{
          href: '/',
          title: 'Willkommen'
        }, {
          title: 'Fahrschule',
          navs: [
            {
              href: '/filialen',
              title: 'Filialen'
            }, {
              href: '/team',
              title: 'Team'
            }, {
              href: '/fahrzeuge',
              title: 'Fahrzeuge'
            }]
        }, {
          title: 'Ausbildung',
          navs: [
            {
              href: '/ausbildungsklassen',
              title: 'Ausbildungsklassen'
            }, {
              href: '/kurse',
              title: 'Kurse'
            }, {
              href: '/bestanden',
              title: 'Bestanden'
            }
          ]
        },{
          href: '/eltern',
          title: 'Eltern'
      }]}
  />

<SocialPlugin/>

{children()}

<Footer/>
</div>

export default Layout