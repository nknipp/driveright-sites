'use strict'

import React from 'react'

import Navbar from '../../../components/navbar-fixed-top'
import Footer from '../../../components/footer'
import SocialPlugin from '../../../components/social-plugins'
import FbRatingStatistics from '../../../components/fb-rating-statistics'

import Logo from '../images/logo.png'

const Layout = ({ children }) =>
  <div>
    <Navbar
      logo={Logo}
      navbarClass='navbar-inverse'
      navs={[{
        href: '/',
        title: 'Willkommen'
      },{
        title: 'Fahrschule',
        navs: [
          {
            href: '/filialen',
            title: 'Filialen'
          }, {
            href: '/team',
            title: 'Team'
          }, {
            href: '/fahrzeuge',
            title: 'Fahrzeuge'
          }]
      },{
        title: 'Ausbildung',
        navs: [
          {
            href: '/ausbildungsklassen',
            title: 'Ausbildungsklassen'
          }, {
            href: '/kurse',
            title: 'Kurse'
          }, {
            href: '/bestanden',
            title: 'Bestanden'
          }, {
            href: '/online-anmeldung',
            title: 'Online-Anmeldung'
          }
        ]
      },{
        href: '/vorteile',
        title: 'Vorteile'
      },{
        href: '/bkf',
        title: 'BKF'
      },{
        href: '/eltern',
        title: 'Für Eltern'
      }]}
    >
      <FbRatingStatistics/>
    </Navbar>

    <SocialPlugin/>

    {children()}

    <Footer/>
  </div>

export default Layout
