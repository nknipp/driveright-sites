'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'
import Link from 'gatsby-link'

import RegisterBtn from '../../../components/register-btn'
import Stores from '../../../components/stores'

import WelcomeBgImg from '../images/hintergrund-willkommen.jpg'
import VipPassWelcome from '../images/vip-pass-willkommen.png'
import OneViewBgImg from '../images/hintergrund-auf-einen-blick.jpg'
import TheoryBgImg from '../images/hintergrund-theorieunterricht.jpg'
import LeftBoxImg from '../images/driveacademy-intensiv.jpg'
import PassedBgImg from '../images/hintergrund-bestanden.jpg'
import BkfTrainingBgImg from '../images/hintergrund-bkf-ausbildung.jpg'

const serviceBoxStyles = { height: '318px' }

const Meta = () =>
  <Helmet>
    <title>Fahrschule DriveAcademy - Moderne Führerscheinausbildung mit Freude, Spaß und Zuverlässigkeit</title>
    <meta name="description" content="Die Drive Academy ist mit 2 Standorten: Wehringen und Mühlhausen immer in deiner Nähe und der perfekte Ansprechpartner wenn es um deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wehringen, Führerscheinausbildung in Mühlhausen, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

class Notification extends React.Component {
  componentDidMount() {
    $(document).ready(function() {
      $('#notification-modal').modal()
    })
  }

  render() {
    return (
      <div id="notification-modal" className="modal fade" tabIndex="-1" role="dialog">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
              <h4 className="modal-title"></h4>
            </div>
            <div className="modal-body" style={{fontSize: "18px"}}>
              <p>
                Liebe Kunden,<br/><br/>
                aufgrund der aktuellen Situation müssen wir auf Anordnung der Verwaltungsbehörde unseren gesamten Fahrschulbetrieb –
                also Theorie- und Praxisunterricht – ab sofort bis voraussichtlich 19.04.2020 einstellen.
              </p>
              <p>
                Außerdem finden beim TÜV Süd ab sofort bis ebenfalls voraussichtlich 19.04.2020 vorerst keine Theorie- und Praxisprüfungen
                mehr statt!
              </p>
              <p>
                Sobald sich Änderungen ergeben, werden wir das umgehend an Euch weitergeben!!
              </p>
              <p>
                Wir sind jedoch nach wie vor für Euch telefonisch da unter der Nummer 0160/90 22 93 58 oder zu unseren aktuellen Bürozeiten
                <ul><li>Montag und Mittwoch 14 – 19.00 Uhr unter der Nummer 08207/96 29 59</li></ul>
              </p>
              <p>
                Hier geht es direkt zur <a href="/online-anmeldung">Online-Anmeldung</a>.
              </p>
              <p>
                Wir wünschen euch eine gute Zeit und bleibt gesund!
              </p>
              <p>
                Eure Fahrschule Drive Academy GmbH<br/>
                Mühlhausen und Wehringen
              </p>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-default" data-dismiss="modal">Schließen</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const Welcome = () =>
  <section>
    <div id="bgimage" className="header-image" style={{ backgroundImage: `url(${WelcomeBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-5 col-xs-12">
            <div className="center-content wow fadeInLeft" data-wow-duration="1s">
              <img src={VipPassWelcome} alt="" />
            </div>
          </div>
          <div className="col-sm-7 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Mit Spaß zum Erfolg!</h1>
              <p>Lege Deine Ausbildung in gute und erfahrene Hände! Bei der DriveAcademy steht neben einer sicheren und zuverlässigen Führerscheinausbildung zudem der Spaß während der Zeit in der wir Dich betreuen im Vordergrund!</p>
              <p>Schaue Dich um und zögere nicht Dich bei uns zu melden</p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const OneView = () =>
  <section className="parallax section">
    <div className="wrapsection">
      <div className="parallax-overlay" style={{ backgroundColor: '#fb7005' }}></div>
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <div className="maintitle">
              <h3 className="section-title">Das passende Komplettpaket für Dich und Deinen Start in die Unabhängigkeit!</h3>
              <p className="lead">
                Bei uns findest Du die passenden Rahmenbedingungen für eine sichere und zuverlässige Führerscheinausbildung in entspannter Atmosphäre!
              </p>
            </div>
          </div>
          <div className="col-md-4 col-sm-6">
            <div className="service-box wow zoomIn" data-wow-duration="1.5s" data-wow-delay="0.1s">
              <img className="img-responsive" src={LeftBoxImg}/>
              <h3>Wir wollen es intensiv! - Du doch auch!</h3>
              <div className="text-left">
                <p>
                  Wo sonst? Natürlich bei der DriveAcademy! Unsere Intensivkurse sparen Dir nicht nur viel Zeit, sondern bringen durch interaktive Konzepte zudem viel Spaß!
                </p>
                <p>
                  So schaffst Du innerhalb von 7 Werktagen Deinen kompletten Theoriestoff und bist bereit für die Theorieprüfung!
                </p>
              </div>
              <div className="text-center">
                <RegisterBtn/>
              </div>
              </div>
          </div>
          <div className="col-md-4 col-sm-6">
            <div className="service-box wow flipInY" style={ serviceBoxStyles } data-wow-duration="1.5s" data-wow-delay="0.1s">
              <i className="fa fa-car fa-2x"></i>
              <h3>Moderne Ausbildungsfahrzeuge</h3>
              <p>
                Bei uns fährst Du ausschließlich in geprüften, modernen und zeitgemäßen Ausbildungsfahrzeugen!
              </p>
              <Link to="/fahrzeuge" className="btn btn-info btn-md" style={{ color: '#000' }}>Ausbildungsfahrzeuge</Link>
            </div>
          </div>
          <div className="col-md-4 col-sm-6">
            <div className="service-box wow flipInY" style={ serviceBoxStyles } data-wow-duration="1.5s" data-wow-delay="0.2s">
              <i className="fa fa-cogs fa-2x"></i>
              <h3>Viele Ausbildungsklassen</h3>
              <p>
                Entscheide Dich für eine von vielen Ausbildungsklassen die Du bei uns erlernen kannst!
              </p>
              <Link to="/ausbildungsklassen" className="btn btn-info btn-md" style={{ color: '#000' }}>Ausbildungsklassen</Link>
            </div>
          </div>
          <div className="col-md-4 col-sm-6">
            <div className="service-box wow flipInY" style={ serviceBoxStyles } data-wow-duration="1.5s" data-wow-delay="0.3s">
              <i className="fa fa-map-marker fa-2x"></i>
              <h3>2 Filialen in deiner Region</h3>
              <p>
                Durch unsere zwei Filialen in Wehringen und Mühlhausen sind wir so gut wie immer für Dich da!
              </p>
              <Link to="/filialen" className="btn btn-info btn-md" style={{ color: '#000' }}>Filialen</Link>
            </div>
          </div>
          <div className="col-md-4 col-sm-6">
            <div className="service-box wow flipInY" style={ serviceBoxStyles } data-wow-duration="1.5s" data-wow-delay="0.4s">
              <i className="fa fa-truck fa-2x"></i>
              <h3>BKF Ausbildung</h3>
              <p>
                Wir sind zudem der ideale Ansprechpartner wenn es um eine BKF Ausbildung geht!
              </p>
              <Link to="/bkf" className="btn btn-info btn-md" style={{ color: '#000' }}>BKF Ausbildung</Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Theory = () =>
  <section className="parallax section" style={{ backgroundImage: `url(${TheoryBgImg})`}}>
    <div className="wrapsection">
      <div className="parallax-overlay"></div>
      <div className="container">
        <div className="row">
          <div className="col-md-12 sol-sm-12">
            <div className="maintitle">
              <h3 className="section-title justtitle">Langeweile beim Theorieunterricht?</h3>
              <p className="lead bottom0 wow bounceInUp">
                Nicht bei der DriveAcademy! - Durch spannende Gruppen- und Lernkonzepte ist jede Theorieeinheit ein Highlight!
              </p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Benefits = () =>
  <section className="center-content">
    <div className="container">
      <div className="row">
        <div className="works_content text-center">
          <h3>Viele Gründe, um Deinen Führerschein bei uns zu machen!</h3>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="single_works_text wow fadeInLeft" data-wow-duration=".5s">
              <i className="fa fa-magic"></i>
              <h3>Mentale Untersützung</h3>
              <p>Hast Du Angst vor dem Führerschein? Angst vor der Prüfung? Wir können Dir mit mentaler Untersützung (Hypnose) helfen.</p>
            </div>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="single_works_text wow fadeInLeft" data-wow-duration="1.5s">
              <i className="fa fa-rocket"></i>
              <h3>Intensivkurse</h3>
              <p>Du hast keine Lust auf endlos langen Theorieunterricht? Dann starte mit uns in 7 Werktagen durch!</p>
            </div>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="single_works_text wow fadeInLeft" data-wow-duration="1.9s">
              <i className="fa fa-expeditedssl"></i>
              <h3>Theorie Garantie</h3>
              <p>Wir lassen Dich nicht hängen! Wenn es bei der ersten Theorieprüfung nicht klappt, bezahlen wir den zweiten Versuch!</p>
            </div>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="single_works_text wow fadeInLeft" data-wow-duration="2.3s">
              <i className="fa fa-code-fork"></i>
              <h3>Automatikausbildung</h3>
              <p>Keine Lust auf manuelle Gangschaltung? Dann melde Dich für die Automatikausbildung an!</p>
            </div>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="single_works_text wow fadeInLeft" data-wow-duration="2.6s">
              <i className="fa fa-smile-o"></i>
              <h3>Handicap-Ausbildung</h3>
              <p>Du hast ein Handicap? Kein Problem! Wir haben für viele Eventualitäten die passende Lösung!</p>
            </div>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="single_works_text wow fadeInLeft" data-wow-duration="2.9s">
              <i className="fa fa-motorcycle"></i>
              <h3>Motorrad-Begleitung</h3>
              <p>Respekt vor schwierigen Fahrmanövern? Bei unserer Fahrbegleitung zeigen wir Dir die passende Fahrlinie!</p>
            </div>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="single_works_text wow fadeInLeft" data-wow-duration="3.2s">
              <i className="fa fa-bus"></i>
              <h3>BKF</h3>
              <p>Bei uns erhältst Du alle erforderlichen und notwendige Aus- und Weiterbildung im BKF-Bereich.</p>
            </div>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="single_works_text wow fadeInLeft" data-wow-duration="3.5s">
              <i className="fa fa-money"></i>
              <h3>Führerscheinfinanzierung</h3>
              <p>Wir bieten Dir mit unserem Partner eine passende Finanzierung für Deinen Führerschein!</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Passed = () =>
  <section id="slider" className="parallax section" style={{ backgroundImage: `url(${PassedBgImg})` }}>
    <div className="wrapsection">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <div id="Carousel" className="carousel slide">
              <ol className="carousel-indicators">
                <li data-target="#Carousel" data-slide-to="0" className="active"></li>
                <li data-target="#Carousel" data-slide-to="1"></li>
                <li data-target="#Carousel" data-slide-to="2"></li>
              </ol>
              <div className="carousel-inner">
                <div className="item active">
                  <blockquote>
                    <p className="lead">
                      Tolle Fahrschule! Sehr empfehlenswert. Perfektes Team. Wunderbare Vorbereitung auf die Prüfung. Vielen Dank!
                    </p>
                    <small>Jana Miller aus Mühlhausen</small>
                  </blockquote>
                </div>
                <div className="item">
                  <blockquote>
                    <p className="lead">
                      War echt toll. Nette Leute dort, die mich zügig zum Führerschein gebracht haben. Klasse!
                    </p>
                    <small>Niklas aus Wehringen</small>
                  </blockquote>
                </div>
                <div className="item">
                  <blockquote>
                    <p className="lead">
                      Hatte riesen Spaß bei der Ausbildung, top Fahrlehrer und heute bestanden. Empfehle ich gerne weiter :)!
                    </p>
                    <small>Maria Roßbach aus Landsberg</small>
                  </blockquote>
                </div>
              </div>
              <a className="left carousel-control" href="#Carousel" data-slide="prev">
                <span className="glyphicon glyphicon-chevron-left"></span>
              </a>
              <a className="right carousel-control" href="#Carousel" data-slide="next">
                <span className="glyphicon glyphicon-chevron-right"></span>
              </a>
            </div>
          </div>
        </div>
        <div className="text-center">
          <RegisterBtn/>
        </div>
      </div>
    </div>
  </section>

const Offices = () =>
  <section>
    <div className="container-fluid">
      <div className="offices-bg" style={{ paddingBottom: '100px' }}>
        <div className="offices_heading_text text-center">
          <h3>Persönlich sind wir in zwei Filialen für Dich zu erreichen!</h3>
          <p>Lust uns persönlich kennen zu lernen? - Dann freuen wir uns auf Deine Kontaktaufnahme!</p>
        </div>
        <Stores
          mode="block"
        />
      </div>
    </div>
  </section>

const BkfTraining = () =>
  <section className="whitecolor parallax section" style={{ backgroundImage: `url(${BkfTrainingBgImg})`, marginBottom: '100px' }}>
    <div className="wrapsection">
      <div className="container">
        <div className="row">
          <div className="col-md-12 sol-sm-12">
            <div className="maintitle">
              <h3 className="section-title justtitle">BKF-Weiterbildung</h3>
              <p className="lead bottom0">
                Wir sind zudem der ideale Ansprechpartner, wenn es um eine BKF-Weiterbildung geht!
              </p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Welcome/>
    <OneView/>
    <Theory/>
    <Benefits/>
    <Passed/>
    <Offices/>
    <BkfTraining/>
  </div>
