'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import RegisterBtn from '../../../components/register-btn'
import Stores from '../../../components/stores'

import OfficesBgImg from '../images/hintergrund-filialen.jpg'

const Meta = () =>
    <Helmet>
        <title>Fahrschule DriveAcademy - Moderne Führerscheinausbildung mit Freude, Spaß und Zuverlässigkeit</title>
        <meta name="description" content="Die Drive Academy ist mit 2 Standorten: Wehringen und Mühlhausen immer in deiner Nähe und der perfekte Ansprechpartner wenn es um deine Führerscheinausbildung geht!"/>
        <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wehringen, Führerscheinausbildung in Mühlhausen, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
    </Helmet>

const Header = () =>
  <section>
    <div id="bgimage" className="header-image" style={{ backgroundImage: `url(${OfficesBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Besuche uns doch einfach!</h1>
              <p>Während unseren Öffnungszeiten sind wir in insgesamt zwei Filialen für Dich zu erreichen. Wir beraten Dich gerne zum Thema Führerschein und beantworten Dir alle offenen Fragen! Zudem kannst Du Dich jederzeit bei uns in der Filiale zu einer Ausbildungsklasse anmelden und deine Führerscheinausbildung sofort beginnen!</p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Offices = () =>
  <section>
    <div className="container-fluid">
      <div className="offices-bg" style={{ paddingBottom: '100px' }}>
        <div className="offices_heading_text text-center">
          <h3>Persönlich sind wir in zwei Filialen für Dich zu erreichen!</h3>
          <p>Lust uns persönlich kennen zu lernen? - Dann freuen wir uns auf Deine Kontaktaufnahme!</p>
        </div>
        <Stores
          mode="block"
        />
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Offices/>
  </div>
