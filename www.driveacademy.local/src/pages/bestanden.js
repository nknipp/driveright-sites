'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import RegisterBtn from '../../../components/register-btn'
import PassedGallery from '../../../components/passed-gallery'

import BgImg from '../images/hintergrund-unterseite-bestanden.jpg'

const Meta = () =>
    <Helmet>
        <title>Fahrschule DriveAcademy - Da ist das Ding!</title>
        <meta name="description" content="Die Drive Academy ist mit 2 Standorten: Wehringen und Mühlhausen immer in deiner Nähe und der perfekte Ansprechpartner wenn es um deine Führerscheinausbildung geht!"/>
        <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wehringen, Führerscheinausbildung in Mühlhausen, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
    </Helmet>

const Header = () =>
  <section>
    <div id="bgimage" className="header-image" style={{ backgroundImage: `url(${BgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Da ist das Ding!</h1>
              <p>Wir gratulieren zur bestanden Führerscheinausbildung und wünschen viel Erfolg und vor allem Spaß mit der neuen Unabhängigkeit!</p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Passed = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="row">
        <PassedGallery renderMode="carousel" wrapperClassName="about-me-text"/>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Passed/>
  </div>
