'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import PrivacyStatement from '../../../components/privacy-statement'

const Meta = () =>
  <Helmet>
    <title>Fahrschule DriveAcademy - Moderne Führerscheinausbildung mit Freude, Spaß und Zuverlässigkeit</title>
    <meta name="description" content="Die Drive Academy ist mit 2 Standorten: Wehringen und Mühlhausen immer in deiner Nähe und der perfekte Ansprechpartner wenn es um deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wehringen, Führerscheinausbildung in Mühlhausen, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

export default () =>
  <div>
    <Meta/>
    <PrivacyStatement/>
  </div>
