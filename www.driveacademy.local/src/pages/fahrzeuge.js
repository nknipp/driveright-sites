'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import Cars from '../../../components/cars'
import RegisterBtn from '../../../components/register-btn'

import CarsBgImg from '../images/hintergrund-fahrzeuge.jpg'

const Meta = () =>
  <Helmet>
    <title>Fahrschule DriveAcademy - Moderne Führerscheinausbildung mit Freude, Spaß und Zuverlässigkeit</title>
    <meta name="description" content="Die Drive Academy ist mit 2 Standorten: Wehringen und Mühlhausen immer in deiner Nähe und der perfekte Ansprechpartner wenn es um deine Führerscheinausbildung geht!"/>
    <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wehringen, Führerscheinausbildung in Mühlhausen, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
  </Helmet>

const Header = () =>
  <section>
    <div id="bgimage" className="header-image" style={{ backgroundImage: `url(${CarsBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Neben der Sicherheit und Zuverlässigkeit bringen unsere Ausbildungsfahrzeuge auch viel Spaß!</h1>
              <p>Eine Sache ist ganz klar, die Zeit mit deinem ersten Auto - in diesem Fall mit deinem Ausbildungsfahrzeug während der Führerscheinausbildung - wirst Du niemals vergessen! Wir möchten Dir diese Zeit noch attraktiver gestalten und bilden aus diesem Grund nur in modernen und zeitgemäßen Ausbildungsfahrzeugen aus.</p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Content = () =>
  <section className="section-space-padding" style={{ backgroundColor: '#f5a568' }}>
    <div className="container">
      <div className="row">
        <Cars/>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Content/>
  </div>

