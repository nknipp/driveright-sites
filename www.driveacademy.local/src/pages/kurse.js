'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import RegisterBtn from '../../../components/register-btn'
import Courses from '../../../components/courses'

import BgImg from '../images/hintergrund-unterseite-kurse.jpg'

const Meta = () =>
    <Helmet>
        <title>Fahrschule DriveAcademy - Wir lieben es kompakt!</title>
        <meta name="description" content="Die Drive Academy ist mit 2 Standorten: Wehringen und Mühlhausen immer in deiner Nähe und der perfekte Ansprechpartner wenn es um deine Führerscheinausbildung geht!"/>
        <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wehringen, Führerscheinausbildung in Mühlhausen, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
    </Helmet>

const Header = () =>
  <section>
    <div id="bgimage" className="header-image" style={{ backgroundImage: `url(${BgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Wir lieben es kompakt!</h1>
              <p>Wir bieten Dir regelmäßige Kurse an. Solltest Du Interesse an einem Kurs haben oder weitere Informationen benötigen, freuen wir uns auf Deine unverbindliche Anfrage.</p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Intro = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="row">
        <div className="col-xs-12 text-center">
          <h3>Hier findest Du den passenden Kurs in ...</h3>
        </div>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Intro/>
    <Courses tags={['', null, 'Intensiv', 'Erste Hilfe', 'Ferien', 'ASF', 'MPU']}/>
  </div>
