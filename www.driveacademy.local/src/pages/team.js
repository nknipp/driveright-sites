'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import Team from '../../../components/team'
import RegisterBtn from '../../../components/register-btn'

import BkfBgImg from '../images/hintergrund-unterseite-team.jpg'

const Meta = () =>
    <Helmet>
        <title>Fahrschule DriveAcademy - DriveAcademy stellt sich vor!</title>
        <meta name="description" content="Die Drive Academy ist mit 2 Standorten: Wehringen und Mühlhausen immer in deiner Nähe und der perfekte Ansprechpartner wenn es um deine Führerscheinausbildung geht!"/>
        <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wehringen, Führerscheinausbildung in Mühlhausen, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
    </Helmet>

const Header = () =>
  <section>
    <div id="bgimage" className="header-image" style={{ backgroundImage: `url(${BkfBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>DriveAcademy stellt sich vor!</h1>
              <p>Deine Führerscheinausbildung ist bei uns in guten Händen. Wir freuen uns darauf mit Dir zusammen viel Spaß zu haben! Denn dafür stehen wir!</p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Content = () =>
  <section className="section-space-padding">
    <div className="container">
      <div className="row">
        <div>
          <Team cssClass="about-me-text" style={{ paddingBottom: '100px' }} />
        </div>
        <div className="col-md-12 text-center">
          <RegisterBtn/>
        </div>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Content/>
  </div>
