'use strict'

import React from 'react'

import ContactRequest from '../../../components/contact-request'
import Stores from '../../../components/stores'

import ContactBgImg from '../images/hintergrund-unterseite-kontakt.jpg'

export default () =>
  <div id="bgimage" className="header-image" style={{ backgroundImage: `url(${ContactBgImg})` }}>
    <div className="container heading-text" style={{ color: '#fff' }}>
      <div className="row">
        <h1>Bock auf den Führerschein? Bock auf uns? Offene Fragen?</h1>
        <p>Der Führerschein und die Ausbildung dazu ist ein großes Thema. Unser Team steht Dir für offene Fragen oder eine Beratung zum Thema Führerscheinausbildung - sehr gerne - zur Verfügung. Rufe uns einfach an, schreibe uns oder schau während unseren Öffnungszeiten in einer unseren Filialen vorbei.</p>
      </div>
      <div className="row" style={{ paddingTop: '40px', paddingBottom: '40px' }}>
        <div className="col-sm-12 col-md-5">
          <ContactRequest/>
        </div>
        <div className="col-sm-12 col-md-6 col-md-offset-1">
          <Stores
            mode="inline"
          />
        </div>
      </div>
    </div>
  </div>
