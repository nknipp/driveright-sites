'use strict'

import React from 'react'

import OnlineRegistration from '../../../components/online-registration'

import OnlineRegistrationBgImg from '../images/hintergrund-unterseite-kontakt.jpg'

export default () =>
  <div id="bgimage" className="header-image" style={{ backgroundImage: `url(${OnlineRegistrationBgImg})` }}>
    <div className="container heading-text" style={{ color: '#fff' }}>
      <div className="row">
        <h1>Bock auf den Führerschein? Bock auf uns? Dann melde Dich jetzt Online an!</h1>
        <p>Vielen Dank für Deine Online-Anmeldung. Damit wir für Dich einen Platz in Deinem Wunschkurs reservieren können, bitten wir Dich um folgende Angaben</p>
      </div>
      <div className="row" style={{ paddingTop: '40px', paddingBottom: '40px' }}>
        <div className="col-sm-12 col-md-12">
          <OnlineRegistration/>
        </div>
      </div>
    </div>
  </div>
