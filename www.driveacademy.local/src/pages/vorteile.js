'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import RegisterBtn from '../../../components/register-btn'

import BkfBgImg from '../images/hintergrund-unterseite-vorteile.jpg'

const Meta = () =>
    <Helmet>
        <title>Fahrschule DriveAcademy - Viele Gründe, um Deinen Führerschein bei uns zu machen!</title>
        <meta name="description" content="Die Drive Academy ist mit 2 Standorten: Wehringen und Mühlhausen immer in deiner Nähe und der perfekte Ansprechpartner wenn es um deine Führerscheinausbildung geht!"/>
        <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wehringen, Führerscheinausbildung in Mühlhausen, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
    </Helmet>

const Header = () =>
  <section>
    <div id="bgimage" className="header-image" style={{ backgroundImage: `url(${BkfBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Viele Gründe, um Deinen Führerschein bei uns zu machen!</h1>
              <p>Wir freuen uns sehr, dass Du Dich über die Vorteile informierst die Du bei einer Führerscheinausbildung bei uns hast. Wir arbeiten nicht nur modern, innovativ und vor allem zeitgemäß, sondern nehmen uns auch gerne für Deine Belange Zeit. Aus diesem Grund stehen wir Dir für Fragen sehr gerne zur Verfügung!</p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Benefits = () =>
  <section className="center-content">
    <div className="container">
      <div className="row">
        <div className="works_content text-center">
          <h3>Viele Gründe, um Deinen Führerschein bei uns zu machen!</h3>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="single_works_text wow fadeInLeft" data-wow-duration=".5s">
              <i className="fa fa-magic"></i>
              <h3>Mentale Untersützung</h3>
              <p>Hast Du Angst vor dem Führerschein? Angst vor der Prüfung? Wir können Dir mit mentaler Untersützung (Hypnose) helfen.</p>
            </div>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="single_works_text wow fadeInLeft" data-wow-duration="1.5s">
              <i className="fa fa-rocket"></i>
              <h3>Intensivkurse</h3>
              <p>Du hast keine Lust auf endlos langen Theorieunterricht? Dann starte mit uns in 7 Werktagen durch!</p>
            </div>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="single_works_text wow fadeInLeft" data-wow-duration="1.9s">
              <i className="fa fa-expeditedssl"></i>
              <h3>Theorie Garantie</h3>
              <p>Wir lassen Dich nicht hängen! Wenn es bei der ersten Theorieprüfung nicht klappt, bezahlen wir den zweiten Versuch!</p>
            </div>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="single_works_text wow fadeInLeft" data-wow-duration="2.3s">
              <i className="fa fa-code-fork"></i>
              <h3>Automatikausbildung</h3>
              <p>Keine Lust auf manuelle Gangschaltung? Dann melde Dich für die Automatikausbildung an!</p>
            </div>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="single_works_text wow fadeInLeft" data-wow-duration="2.6s">
              <i className="fa fa-smile-o"></i>
              <h3>Handicap-Ausbildung</h3>
              <p>Du hast ein Handicap? Kein Problem! Wir haben für viele Eventualitäten die passende Lösung!</p>
            </div>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="single_works_text wow fadeInLeft" data-wow-duration="2.9s">
              <i className="fa fa-motorcycle"></i>
              <h3>Motorrad-Begleitung</h3>
              <p>Respekt vor schwierigen Fahrmanövern? Bei unserer Fahrbegleitung zeigen wir Dir die passende Fahrlinie!</p>
            </div>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="single_works_text wow fadeInLeft" data-wow-duration="3.2s">
              <i className="fa fa-bus"></i>
              <h3>BKF</h3>
              <p>Bei uns erhältst Du alle erforderlichen und notwendige Aus- und Weiterbildung im BKF-Bereich.</p>
            </div>
          </div>
          <div className="col-md-3 col-sm-6 col-xs-12">
            <div className="single_works_text wow fadeInLeft" data-wow-duration="3.5s">
              <i className="fa fa-money"></i>
              <h3>Führerscheinfinanzierung</h3>
              <p>Wir bieten Dir mit unserem Partner eine passende Finanzierung für Deinen Führerschein!</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Benefits/>
  </div>
