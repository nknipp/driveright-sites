'use strict'

import React from 'react'
import { Helmet } from 'react-helmet'

import DrivingLicenceClasses from '../../../components/driving-licence-classes'

import RegisterBtn from '../../../components/register-btn'

import DrivingLicenceClassesBgImg from '../images/hintergrund-ausbildungsklassen.jpg'

const Meta = () =>
    <Helmet>
        <title>Fahrschule DriveAcademy - Moderne Führerscheinausbildung mit Freude, Spaß und Zuverlässigkeit</title>
        <meta name="description" content="Die Drive Academy ist mit 2 Standorten: Wehringen und Mühlhausen immer in deiner Nähe und der perfekte Ansprechpartner wenn es um deine Führerscheinausbildung geht!"/>
        <meta name="keywords" content="Fahren mit 17, Fahrschule, Führerschein, Führerscheinausbildung, Führerscheinausbildung in Wehringen, Führerscheinausbildung in Mühlhausen, Theorieunterricht mit Spaß, Zuverlässige Führerscheinausbildung"/>
    </Helmet>

const Header = () =>
  <section>
    <div id="bgimage" className="header-image" style={{ backgroundImage: `url(${DrivingLicenceClassesBgImg})` }}>
      <div className="container">
        <div className="row">
          <div className="col-sm-8 col-xs-12 heading-text">
            <div className="wow zoomIn" data-wow-duration="1s">
              <h1>Für jedes Alter und jeden Fahrzeugtyp die passende Ausbildungsklasse!</h1>
              <p>Führerschein ist nicht gleich Führerschein! Was für die meisten in Zusammenhang mit einem Auto steht, gibt es in vielen verschiedenen Variationen. Nehm Dir einen Augenblick Zeit und schaue Dir in Ruhe an, in welcher Ausbildungsklasse wir Dich ausbilden können. Bei Fragen sind wir sehr gerne für Dich da!</p>
              <RegisterBtn/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

const Licences = () =>
  <section>
    <div className="container">
      <div className="works_content">
        <DrivingLicenceClasses
          classes={['b','be','b96','mofa','am','a1','a2','a','c1','c1e','c','ce','l','t']}
        />
      </div>
    </div>
  </section>

export default () =>
  <div>
    <Meta/>
    <Header/>
    <Licences/>
  </div>
