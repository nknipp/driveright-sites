'use strict'

import React from 'react'

export default ({ id }) =>
  <span className="navbar-rating">
    <div id="wkdb-widget"></div>
    <script type="text/javascript" src={`https://www.wkdb-siegel.de/v1/widget-${id}.js`} async></script>
  </span>
