import React from 'react'

let lastId = 0;

class GoogleMap extends React.Component {
  constructor(props) {
    super(props)
  }

  componentWillMount() {
    lastId++
    this.mapsId = `gmap${lastId}`
  }

  componentDidMount() {
    const map = new google.maps.Map(document.getElementById(this.mapsId), {
      zoom: parseInt(this.props.zoom)
    })
    const image = this.props.image
    const geocoder = new google.maps.Geocoder()
    geocoder.geocode({ address: this.props.address }, function(results, status) {
      if (status === 'OK') {
        map.setCenter(results[0].geometry.location)
        const marker = new google.maps.Marker({
          map: map,
          position: results[0].geometry.location,
          icon: image
        })
      } else {
        console.log('Geocode was not successful for the following reason: ' + status)
      }
    })
  }

  render() {
    return (
      <div id={this.mapsId} style={{ height: this.props.height, width: this.props.width}}/>
    )
  }
}

export default GoogleMap