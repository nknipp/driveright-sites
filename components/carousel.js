import React from 'react'

class Carousel extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const defaults = {
      items: 1,
      loop: true,
      margin: 10,
      autoplay: true,
      autoplayHoverPause: false
    }
    const options = Object.assign({}, defaults, this.props)
    $('.owl-carousel').owlCarousel(options)
  }

  render() {
    return (
      <div className="owl-carousel owl-theme">
        {this.props.children}
      </div>
    )
  }
}

export default Carousel