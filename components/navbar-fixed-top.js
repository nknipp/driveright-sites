import React from 'react'
import Link from 'gatsby-link'

class Navbar extends React.Component {
  constructor(props) {
    super(props)
    this.navIndex = 0
  }

  componentDidMount() {
    $('ul.nav.navbar-nav li a[href="' + document.location.pathname + '"]').parent().addClass('active')
  }

  static handleClick(event) {
    $('ul.nav.navbar-nav li').removeClass('active')
    $(event.currentTarget).parent().addClass('active')
  }

  renderNavItem(navItem) {
    this.navIndex += 1
    if (navItem.navs) {
      const navItemIdx = this.navIndex
      this.navIndex += 1
      return (
        <li key={navItemIdx} className="dropdown">
          <a href="#" className="dropdown-toggle" data-toggle="dropdown">{navItem.title}
            &nbsp;<b className="caret"></b>
          </a>
          <ul className="dropdown-menu" key={this.navIndex}>
            { navItem.navs.map(nav => this.renderNavItem(nav)) }
          </ul>
        </li>
      );
    } else {
      return <li key={this.navIndex}><Link to={navItem.href} onClick={Navbar.handleClick} data-toggle="collapse" data-target=".navbar-collapse.in">{navItem.title}</Link></li>
    }
  }

  render() {
    const { children, logo, navs, navbarClass } = this.props
    return (
      <header id="home" className={`header navbar ${navbarClass} navbar-fixed-top main-menu`}>
        <div className="container">
          <div className="navbar-header">
            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
              <span className="icon-bar"></span>
            </button>
            <Link to="/" className="navbar-brand"><img className="img-responsive" src={logo} alt="Logo" /></Link>
            {children}
          </div>
          <div className="collapse navbar-collapse">
            <ul className="nav navbar-nav navbar-right">
              { navs.map(nav => this.renderNavItem(nav)) }
            </ul>
          </div>
        </div>
      </header>
    )
  }
}

export default Navbar
