import React from 'react'

const ParallaxBackgorund = ({ backgroundImage, children }) =>
  <section className="parallax" style={{ backgroundImage: `url(${backgroundImage})`}}>
    <div className="container">
      <div className="row">
        <div className="col-md-12 sol-sm-12">
          {children}
        </div>
      </div>
    </div>
  </section>

export default ParallaxBackgorund