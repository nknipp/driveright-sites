'use strict'

import React from 'react'

class PassedGallery extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      gallery: []
    }
  }

  componentDidMount() {
    $.getJSON('/passed-gallery.json', (data) => {
      this.setState({ gallery: data })

      const defaults = {
        items: 1,
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayHoverPause: false
      }
      const options = Object.assign({}, defaults, this.props)
      $('.owl-carousel').owlCarousel(options)
    });
  }

  renderRotated() {
    return (
      <div id="passed-gallery">
        { this.state.gallery.map((passedItem, idx) => {
          const rotate = Math.floor((Math.random() * 10) + 1) * (Math.random() < 0.5 ? -1 : 1);
          return (
            <div style={{ transform: `rotate(${rotate}deg)` }}>
              <img className="img-responsive" src={`/images/passed-gallery/${passedItem.filename}`} alt={passedItem.name}/>
              <div className="carousel-caption">
                <div className="carousel-caption-block">
                  <h3>{passedItem.name}</h3>
                  <p>{passedItem.date}</p>
                  <p>{passedItem.text}</p>
                </div>
              </div>
            </div>
          )
        })}
      </div>
    )
  }

  renderCarousel() {
    return (
      <div id="passed-gallery" className="owl-carousel owl-theme">
        { this.state.gallery.map((passedItem) => {
          return (
            <div className="item" key={passedItem.filename}>
              <div className={`col-md-8 text-center center-block ${this.props.wrapperClassName ? this.props.wrapperClassName : ''}`} style={{ float: 'none' }}>
                <img className="img-responsive" src={`/images/passed-gallery/${passedItem.filename}`} alt={passedItem.name}/>
                <h3>{passedItem.name}</h3>
                <p>{passedItem.date}</p>
                <p>{passedItem.text}</p>
              </div>
            </div>
          )
        })}
      </div>
    )
  }

  render() {
    switch (this.props.renderMode) {
      case 'rotate':
        return this.renderRotated()

      default:
      case 'carousel':
        return this.renderCarousel()
    }
  }
}

export default PassedGallery