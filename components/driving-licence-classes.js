'use strict'

import React from 'react'

import ClassA from '../resources/images/ausbildungsklasse-a.png'
import ClassA1 from '../resources/images/ausbildungsklasse-a1.png'
import ClassA2 from '../resources/images/ausbildungsklasse-a2.png'
import ClassAM from '../resources/images/ausbildungsklasse-am.png'
import ClassB from '../resources/images/ausbildungsklasse-b.png'
import ClassBE from '../resources/images/ausbildungsklasse-be.png'
import ClassB96 from '../resources/images/ausbildungsklasse-b96.png'
import ClassC from '../resources/images/ausbildungsklasse-c.png'
import ClassC1 from '../resources/images/ausbildungsklasse-c1.png'
import ClassC1E from '../resources/images/ausbildungsklasse-c1e.png'
import ClassCE from '../resources/images/ausbildungsklasse-ce.png'
import ClassD1 from '../resources/images/ausbildungsklasse-d1.png'
import ClassD1E from '../resources/images/ausbildungsklasse-d1e.png'
import ClassD from '../resources/images/ausbildungsklasse-d.png'
import ClassDE from '../resources/images/ausbildungsklasse-de.png'
import ClassL from '../resources/images/ausbildungsklasse-l.png'
import ClassMofa from '../resources/images/ausbildungsklasse-mofa.png'
import ClassT from '../resources/images/ausbildungsklasse-t.png'

import Set2A from '../resources/images/set2/ausbildungsklasse-a.png'
import Set2A1 from '../resources/images/set2/ausbildungsklasse-a1.png'
import Set2A2 from '../resources/images/set2/ausbildungsklasse-a2.png'
import Set2AM from '../resources/images/set2/ausbildungsklasse-am.png'
import Set2B from '../resources/images/set2/ausbildungsklasse-b.png'
import Set2BE from '../resources/images/set2/ausbildungsklasse-be.png'
import Set2B96 from '../resources/images/set2/ausbildungsklasse-b96.png'
import Set2C from '../resources/images/set2/ausbildungsklasse-c.png'
import Set2C1 from '../resources/images/set2/ausbildungsklasse-c1.png'
import Set2C1E from '../resources/images/set2/ausbildungsklasse-c1e.png'
import Set2CE from '../resources/images/set2/ausbildungsklasse-ce.png'
import Set2D1 from '../resources/images/set2/ausbildungsklasse-d1.png'
import Set2D1E from '../resources/images/set2/ausbildungsklasse-d1e.png'
import Set2D from '../resources/images/set2/ausbildungsklasse-d.png'
import Set2DE from '../resources/images/set2/ausbildungsklasse-de.png'
import Set2L from '../resources/images/set2/ausbildungsklasse-l.png'
import Set2Mofa from '../resources/images/set2/ausbildungsklasse-mofa.png'
import Set2T from '../resources/images/set2/ausbildungsklasse-t.png'

const boxStyles = {
  backgroundColor: '#fff',
  width: '262px',
  height: '542px',
  marginLeft: 'auto',
  marginRight: 'auto',
  border: '1px solid #ddd',
  transition: '.5s',
  marginBottom: '30px'
}
const textBoxStyles = { padding: '10px' }

export default ({ classes, imageSet }) =>
  <div className="row">
    { classes.indexOf('a') !== -1 ?
      <div className="col-sm-6 col-md-3">
        <div className="box wow fadeInLeft" style={boxStyles} data-wow-duration="1.5s">
          <img className="img-responsive" src={imageSet === 'set2' ? Set2A : ClassA}/>
          <div style={textBoxStyles}>
            <p><strong>Mindestalter: </strong>24 Jahre (für Direkteinstieg), 20 Jahre (bei mind. 2 Jahre Vorbesitz der Klasse A2)</p>
            <p><strong>Eingeschlossene Klasse: </strong>A2, A1 und AM</p>
          </div>
        </div>
      </div>
      : ''
    }
    { classes.indexOf('a2') !== -1 ?
      <div className="col-sm-6 col-md-3">
        <div className="box wow fadeInLeft" style={boxStyles} data-wow-duration="1.5s">
          <img className="img-responsive"  src={imageSet === 'set2' ? Set2A2 : ClassA2}/>
          <div style={textBoxStyles}>
            <p><strong>Mindestalter: </strong>18 Jahre</p>
            <p><strong>Eingeschlossene Klasse: </strong>A und AM</p>
          </div>
        </div>
      </div>
      : ''
    }
    { classes.indexOf('a1') !== -1 ?
      <div className="col-sm-6 col-md-3">
        <div className="box wow fadeInLeft" style={boxStyles} data-wow-duration="1.5s">
          <img className="img-responsive" src={imageSet === 'set2' ? Set2A1 : ClassA1}/>
          <div style={textBoxStyles}>
            <p><strong>Mindestalter: </strong>16 Jahre</p>
            <p><strong>Eingeschlossene Klasse: </strong>AM</p>
          </div>
        </div>
      </div>
      : ''
    }
    { classes.indexOf('am') !== -1 ?
      <div className="col-sm-6 col-md-3">
        <div className="box wow fadeInLeft" style={boxStyles} data-wow-duration="1.5s">
          <img className="img-responsive" src={imageSet === 'set2' ? Set2AM : ClassAM}/>
          <div style={textBoxStyles}>
            <p><strong>Mindestalter: </strong>16 Jahre</p>
            <p><strong>Eingeschlossene Klasse: </strong>Keine</p>
          </div>
        </div>
      </div>
      : ''
    }
    { classes.indexOf('mofa') !== -1 ?
      <div className="col-sm-6 col-md-3">
        <div className="box wow fadeInLeft" style={boxStyles} data-wow-duration="1.5s">
          <img className="img-responsive" src={imageSet === 'set2' ? Set2Mofa : ClassMofa}/>
          <div style={textBoxStyles}>
            <p><strong>Mindestalter: </strong>15 Jahre</p>
            <p><strong>Eingeschlossene Klasse: </strong>Keine</p>
          </div>
        </div>
      </div>
      : ''
    }
    { classes.indexOf('b') !== -1 ?
      <div className="col-sm-6 col-md-3">
        <div className="box wow fadeInLeft" style={boxStyles} data-wow-duration="1.5s">
          <img className="img-responsive" src={imageSet === 'set2' ? Set2B : ClassB}/>
          <div style={textBoxStyles}>
            <p><strong>Mindestalter: </strong>18 Jahre (17 Jahre für Teilnehmer am "Begleiteten Fahren mit 17")</p>
            <p><strong>Eingeschlossene Klasse: </strong>AM und L</p>
          </div>
        </div>
      </div>
      : ''
    }
    { classes.indexOf('be') !== -1 ?
      <div className="col-sm-6 col-md-3">
        <div className="box wow fadeInLeft" style={boxStyles} data-wow-duration="1.5s">
          <img className="img-responsive" src={imageSet === 'set2' ? Set2BE : ClassBE}/>
          <div style={textBoxStyles}>
            <p><strong>Mindestalter: </strong>18 Jahre (17 Jahre für Teilnehmer am "Begleiteten Fahren mit 17"), Vorbesitz Führerscheinklasse B notwendig</p>
            <p><strong>Eingeschlossene Klasse: </strong>Keine</p>
          </div>
        </div>
      </div>
      : ''
    }
    { classes.indexOf('b96') !== -1 ?
      <div className="col-sm-6 col-md-3">
        <div className="box wow fadeInLeft" style={boxStyles} data-wow-duration="1.5s">
          <img className="img-responsive" src={imageSet === 'set2' ? Set2B96 : ClassB96}/>
          <div style={textBoxStyles}>
            <p><strong>Mindestalter: </strong>18 Jahre (17 Jahre für Teilnehmer am „Begleiteten Fahren mit 17“), Vorbesitz Führerscheinklasse B notwendig</p>
            <p><strong>Eingeschlossene Klasse: </strong>Keine</p>
          </div>
        </div>
      </div>
      : ''
    }
    { classes.indexOf('c1') !== -1 ?
      <div className="col-sm-6 col-md-3">
        <div className="box wow fadeInLeft" style={boxStyles} data-wow-duration="1.5s">
          <img className="img-responsive" src={imageSet === 'set2' ? Set2C1 : ClassC1}/>
          <div style={textBoxStyles}>
            <p><strong>Mindestalter: </strong>18 Jahre, Vorbesitz Führerscheinklasse B notwendig</p>
            <p><strong>Eingeschlossene Klasse: </strong>Keine</p>
          </div>
        </div>
      </div>
      : ''
    }
    { classes.indexOf('c1e') !== -1 ?
      <div className="col-sm-6 col-md-3">
        <div className="box wow fadeInLeft" style={boxStyles} data-wow-duration="1.5s">
          <img className="img-responsive" src={imageSet === 'set2' ? Set2C1E : ClassC1E}/>
          <div style={textBoxStyles}>
            <p><strong>Mindestalter: </strong>18 Jahre, Vorbesitz Führerscheinklasse C1 notwendig</p>
            <p><strong>Eingeschlossene Klasse: </strong>BE sowie D1E, sofern der Inhaber zum Führen von Fahrzeugen der Klasse D1 berechtigt ist</p>
          </div>
        </div>
      </div>
      : ''
    }
    { classes.indexOf('c') !== -1 ?
      <div className="col-sm-6 col-md-3">
        <div className="box wow fadeInLeft" style={boxStyles} data-wow-duration="1.5s">
          <img className="img-responsive" src={imageSet === 'set2' ? Set2C : ClassC}/>
          <div style={textBoxStyles}>
            <p><strong>Mindestalter: </strong>21 Jahre, Vorbesitz Führerscheinklasse B notwendig</p>
            <p><strong>Eingeschlossene Klasse: </strong>Keine</p>
          </div>
        </div>
      </div>
      : ''
    }
    { classes.indexOf('ce') !== -1 ?
      <div className="col-sm-6 col-md-3">
        <div className="box wow fadeInLeft" style={boxStyles} data-wow-duration="1.5s">
          <img className="img-responsive" src={imageSet === 'set2' ? Set2CE : ClassCE}/>
          <div style={textBoxStyles}>
            <p><strong>Mindestalter: </strong>21 Jahre, Vorbesitz Führerscheinklasse C notwendig</p>
            <p><strong>Eingeschlossene Klasse: </strong>BE, C1E und T sowie D1E, sofern der Inhaber zum Führen von Fahrzeugen der Klasse D1 berechtigt ist und DE, sofern er zum Führen von Fahrzeugen der Klasse D berechtigt ist</p>
          </div>
        </div>
      </div>
      : ''
    }
    { classes.indexOf('d1') !== -1 ?
      <div className="col-sm-6 col-md-3">
        <div className="box wow fadeInLeft" style={boxStyles} data-wow-duration="1.5s">
          <img className="img-responsive" src={imageSet === 'set2' ? Set2D1 : ClassD1}/>
          <div style={textBoxStyles}>
            <p><strong>Mindestalter: </strong>21 Jahre, Vorbesitz Führerscheinklasse B notwendig</p>
            <p><strong>Eingeschlossene Klasse: </strong>Keine</p>
          </div>
        </div>
      </div>
      : ''
    }
    { classes.indexOf('d1e') !== -1 ?
      <div className="col-sm-6 col-md-3">
        <div className="box wow fadeInLeft" style={boxStyles} data-wow-duration="1.5s">
          <img className="img-responsive" src={imageSet === 'set2' ? Set2D1E : ClassD1E}/>
          <div style={textBoxStyles}>
            <p><strong>Mindestalter: </strong>21 Jahre, Vorbesitz Führerscheinklasse D1 notwendig</p>
            <p><strong>Eingeschlossene Klasse: </strong>BE</p>
          </div>
        </div>
      </div>
      : ''
    }
    { classes.indexOf('d') !== -1 ?
      <div className="col-sm-6 col-md-3">
        <div className="box wow fadeInLeft" style={boxStyles} data-wow-duration="1.5s">
          <img className="img-responsive" src={imageSet === 'set2' ? Set2D : ClassD}/>
          <div style={textBoxStyles}>
            <p><strong>Mindestalter: </strong>24 Jahre, Vorbesitz Führerscheinklasse B notwendig</p>
            <p><strong>Eingeschlossene Klasse: </strong>D1</p>
          </div>
        </div>
      </div>
      : ''
    }
    { classes.indexOf('de') !== -1 ?
      <div className="col-sm-6 col-md-3">
        <div className="box wow fadeInLeft" style={boxStyles} data-wow-duration="1.5s">
          <img className="img-responsive" src={imageSet === 'set2' ? Set2DE : ClassDE}/>
          <div style={textBoxStyles}>
            <p><strong>Mindestalter: </strong>24 Jahre, Vorbesitz Führerscheinklasse D notwendig</p>
            <p><strong>Eingeschlossene Klasse: </strong>BE, D1E</p>
          </div>
        </div>
      </div>
      : ''
    }
    { classes.indexOf('l') !== -1 ?
      <div className="col-sm-6 col-md-3">
        <div className="box wow fadeInLeft" style={boxStyles} data-wow-duration="1.5s">
          <img className="img-responsive" src={imageSet === 'set2' ? Set2L : ClassL}/>
          <div style={textBoxStyles}>
            <p><strong>Mindestalter: </strong>16 Jahre </p>
            <p><strong>Eingeschlossene Klasse: </strong>Keine</p>
          </div>
        </div>
      </div>
      : ''
    }
    { classes.indexOf('t') !== -1 ?
      <div className="col-sm-6 col-md-3">
        <div className="box wow fadeInLeft" style={boxStyles} data-wow-duration="1.5s">
          <img className="img-responsive" src={imageSet === 'set2' ? Set2T : ClassT}/>
          <div style={textBoxStyles}>
            <p><strong>Mindestalter: </strong>16 Jahre bei 40 km/h (bbH) und 18 Jahre bei 60 km/h (bbH)</p>
            <p><strong>Eingeschlossene Klasse: </strong>L, AM</p>
          </div>
        </div>
      </div>
      : ''
    }
  </div>
