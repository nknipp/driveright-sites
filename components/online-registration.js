'use strict'

import React from 'react'

class OnlineRegistration extends React.Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
    this.handleCheck = this.handleCheck.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this)
    this.state = {
      lastname: '',
      firstname: '',
      street: '',
      zipcode: '',
      city: '',
      birthdate: '',
      emailAddress: '',
      phone: '',
      interestedIn: '',
      licence: '',
      owningLicences: '',
      startDate: '',
      endDate: ''
    }
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleCheck() {
    let interestedInVal = '';
    const cbs = $('#onlineRegistrationForm').find('input:checked');
    cbs.each(function (idx, elem) {
      interestedInVal += $(elem).val() + ' ';
    })
    this.setState({
      interestedIn: interestedInVal.trim()
    });
  }

  handleSubmit() {
    const serializedData = `{
      "query": "mutation { sendOnlineRegistration(lastname: \\"${this.state.lastname}\\", firstname: \\"${this.state.firstname}\\", street: \\"${this.state.street}\\", zipcode: \\"${this.state.zipcode}\\", city: \\"${this.state.city}\\", birthdate: \\"${this.state.birthdate}\\", phone: \\"${this.state.phone}\\", emailAddress: \\"${this.state.emailAddress}\\", whereToFindUs: \\"${this.state.interestedIn}\\", licence: \\"${this.state.licence}\\", owningLicences: \\"${this.state.owningLicences}\\", startDate: \\"${this.state.startDate}\\", endDate: \\"${this.state.endDate}\\") { success errors }}",
      "variables": null
    }`
    $.ajax({
      url: "https://api.drive-right.de/q",
      method: "POST",
      contentType: "application/json",
      data: serializedData,
      success: function(data) {
        if (!data.data.sendOnlineRegistration.success) {
          let errorMessage = '<ul>';
          data.data.sendOnlineRegistration.errors.forEach(function(err) {
            errorMessage += '<li>' + err + '</li>';
          });
          errorMessage += '</ul>';
          $('#onlineRegistrationMessages').show().html(errorMessage);
        } else {
          $('#onlineRegistrationMessages').hide().html('');
          $('#onlineRegistrationForm').hide();
          $('#onlineRegistrationSuccess').show();
        }
      }
    });
  }

  render() {
    return (
      <div className="contact-form wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
          <div className="row">
            <div className="col-sm-8">
              <form id="onlineRegistrationForm" name="onlineRegistrationForm" method="post" action="#">
                <div id="onlineRegistrationMessages" style={{ display: "none" }} className="col-sm-10"></div>
                <div className="row wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                  <div className="form-group">
                    <div className="row">
                      <div className="col-sm-12 col-md-6">
                        <input type="text" name="firstname" className="form-control" placeholder="Vorname" required="required" onChange={this.handleChange}/>
                      </div>
                      <div className="col-sm-12 col-md-6">
                        <input type="text" name="lastname" className="form-control" placeholder="Nachname" required="required" onChange={this.handleChange}/>
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="row">
                      <div className="col-sm-12 col-md-12">
                        <input type="text" name="street" className="form-control" placeholder="Straße" required="required" onChange={this.handleChange}/>
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="row">
                      <div className="col-sm-2">
                        <input type="text" name="zipcode" className="form-control" placeholder="PLZ" required="required" onChange={this.handleChange}/>
                      </div>
                      <div className="col-sm-10">
                        <input type="text" name="city" className="form-control" placeholder="Ort" required="required" onChange={this.handleChange}/>
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="row">
                      <div className="col-sm-12 col-md-12">
                        <label>Geburtsdatum</label>
                      </div>
                      <div className="col-sm-4">
                        <input type="date" name="birthdate" className="form-control" placeholder="Geburtsdatum" required="required" onChange={this.handleChange}/>
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <input type="tel" name="phone" className="form-control" placeholder="Telefon" required="required" onChange={this.handleChange}/>
                  </div>
                  <div className="form-group">
                    <input type="email" name="emailAddress" className="form-control" placeholder="E-Mail Adresse" required="required" onChange={this.handleChange}/>
                  </div>
                  <div className="form-group interested-in">
                    <div className="row">
                      <div className="col-sm-12 col-md-12">
                        <label>Wie bist Du auf uns aufmerksam geworden?</label>
                      </div>
                      <div className="col-sm-12 col-md-3">
                        <input type="checkbox" name="interestedIn" value="Empfehlung" onClick={this.handleCheck} style={{ width: "20px", height: "20px" }}/> <label style={{ verticalAlign: "bottom" }}>Empfehlung</label>
                      </div>
                      <div className="col-sm-12 col-md-3">
                        <input type="checkbox" name="interestedIn" value="Internetseite" onClick={this.handleCheck} style={{ width: "20px", height: "20px" }}/> <label style={{ verticalAlign: "bottom" }}>Internetseite</label>
                      </div>
                      <div className="col-sm-12 col-md-3">
                        <input type="checkbox" name="interestedIn" value="Social Media" onClick={this.handleCheck} style={{ width: "20px", height: "20px" }}/> <label style={{ verticalAlign: "bottom" }}>Social Media</label>
                      </div>
                      <div className="col-sm-12 col-md-3">
                        <input type="checkbox" name="interestedIn" value="Sonstiges" onClick={this.handleCheck} style={{ width: "20px", height: "20px" }}/> <label style={{ verticalAlign: "bottom" }}>Sonstiges</label>
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <input type="text" name="licence" className="form-control" placeholder="Welche Führerscheinklasse möchtest Du bei uns machen?" required="required" onChange={this.handleChange}/>
                  </div>
                  <div className="form-group">
                    <input type="text" name="owningLicences" className="form-control" placeholder="Hast Du bereits einen Führerschein? Wenn ja, welchen?" required="required" onChange={this.handleChange}/>
                  </div>
                  <div className="form-group">
                    <div className="row">
                      <div className="col-sm-12 col-md-12">
                        <label>Wann möchtest Du starten?</label>
                      </div>
                      <div className="col-sm-4">
                        <input type="date" name="startDate" className="form-control" placeholder="Wann möchtest Du starten?" required="required" onChange={this.handleChange}/>
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="row">
                      <div className="col-sm-12 col-md-12">
                        <label>Wann möchtest Du fertig sein?</label>
                      </div>
                      <div className="col-sm-4">
                        <input type="date" name="endDate" className="form-control" placeholder="Wann möchtest Du fertig sein?" required="required" onChange={this.handleChange}/>
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <button type="button" className="btn btn-primary btn-lg" onClick={this.handleSubmit}>Absenden</button>
                  </div>
                </div>
              </form>
              <div id="onlineRegistrationSuccess" className="contact-request-success" style={{ display: "none" }}>
                <h3>Vielen Dank für ihre Anmeldung. Wir werden uns mit Dir nochmals in Verbindung setzen um alles weitere im Detail zu klären. So musst Du erst am ersten Tag Deiner Ausbildung bei uns persönlich vorbeischauen.</h3>
              </div>
            </div>
          </div>
        </div>
    )
  }
}

export default OnlineRegistration
