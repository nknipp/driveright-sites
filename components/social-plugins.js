'use strict'

import React from 'react'
import Link from "gatsby-link"

class SocialPlugins extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      drivingSchool: {
        address: {}
      }
    }
  }

  componentDidMount() {
    $.getJSON('/drivingSchool.json', (data) => {
      this.setState({ drivingSchool: data })
    });
  }

  render() {
    const { facebookId, instagramId, phone, onlineLearning } = this.state.drivingSchool
    let facebookText = ''
    if (facebookId) {
      facebookText = <div className="sticky-item"><a href={`https://www.facebook.com/${facebookId}`} title="Besuche uns auf Facebook!"><i className="fa fa-facebook-square fa-3x"></i></a></div>
    }
    let instagramText = ''
    if (instagramId) {
      instagramText = <div className="sticky-item"><a href={`https://www.instagram.com/${instagramId}`} title="Besuche uns auf Instagram!"><i className="fa fa-instagram fa-3x"></i></a></div>
    }
    let onlineLearningText = ''
    if (onlineLearning) {
      onlineLearningText =  <div className="sticky-item"><a href={onlineLearning} title="Online-Lernen!"><i className="fa fa-graduation-cap fa-3x"></i></a></div>
    }

    return (
      <div className="sticky-container">
        <div className="sticky-item"><a href={`tel:${phone}`} title="Ruf uns an!"><i className="fa fa-phone-square fa-3x"></i></a></div>
        <div className="sticky-item"><Link to="/kontakt" title="Schreibe uns!"><i className="fa fa-envelope fa-3x"></i></Link></div>
        {facebookText}
        {instagramText}
        {onlineLearningText}
      </div>
    )
  }
}
export default SocialPlugins