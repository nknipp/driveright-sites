'use strict'

/**
 * Properties:
 * mode: 'block' oder 'inline'
 *    Anzeigemodus
 *    block: bootstrap container mit 4-col vertikalem Block
 *    inline: content einer Spalte
 *
 * openingHoursTitle: string
 *    Titel für die Öffnungszeiten
 *    Standard: Öffnungszeiten Beratung & Anmeldung
 *
 * theoreticalLessonsTitle: string
 *    Titel für die Öffnungszeiten Theoretischer Unterricht
 *    Standard: Öffnungszeiten Theoretischer Unterricht
 *
 * appointmentByAgreement: bool
 *    "sowie nach Vereinbarung" anzeigen
 *    Standard: false/undefined
 */

import React from 'react'

class Stores extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      stores: []
    }
  }

  componentDidMount() {
    $.getJSON('/stores.json', (data) => {
      this.setState({ stores: data })
    });
  }

  static renderDay(day, openingHours) {
    if (openingHours.from1 && openingHours.to1) {
      return (
        <div className="table-row">
          <div className="table-cell day-cell">{day}</div>
          <div className="table-cell">
            <span>{openingHours.from1} Uhr - {openingHours.to1} Uhr</span>
            {openingHours.from2 ? <span> &<br/>{openingHours.from2} Uhr - {openingHours.to2} Uhr</span> : ''}
          </div>
        </div>
      )
    }
    return ''
  }

  static renderWeek(openingHours) {
    return (
      <div className="table">
        {Stores.renderDay('Montag', openingHours.monday)}
        {Stores.renderDay('Dienstag', openingHours.tuesday)}
        {Stores.renderDay('Mittwoch', openingHours.wednesday)}
        {Stores.renderDay('Donnerstag', openingHours.thursday)}
        {Stores.renderDay('Freitag', openingHours.friday)}
        {Stores.renderDay('Samstag', openingHours.saturday)}
      </div>
    )
  }

  static isOpenHoursEmpty(openingHours) {
    for (const day of Object.keys(openingHours)) {
      for (const time of Object.keys(openingHours[day])) {
        if (openingHours[day][time]) return false;
      }
    }
    return true;
  }

  renderOpeningHours(store) {
    const showOpeningHours = !Stores.isOpenHoursEmpty(store.openingHours)
    let openingHoursText = ''
    if (showOpeningHours) {
      openingHoursText =
        <div>
          <p>{this.props.openingHoursTitle ? this.props.openingHoursTitle : 'Öffnungszeiten Beratung & Anmeldung'}</p>
          {Stores.renderWeek(store.openingHours)}
        </div>
    }

    let theoreticalLessonsText = ''
    if (!Stores.isOpenHoursEmpty(store.theoreticalLessons)) {
      theoreticalLessonsText =
        <div style={ showOpeningHours ? { paddingTop: '20px' } : {}}>
          <p>{this.props.theoreticalLessonsTitle ? this.props.theoreticalLessonsTitle : 'Öffnungszeiten Theoretischer Unterricht'}</p>
          {Stores.renderWeek(store.theoreticalLessons)}
        </div>
    }

    return (
      <div>
        {openingHoursText}
        {theoreticalLessonsText}
        <div>
          <br/>
          {this.props.appointmentByAgreement ? <span>sowie nach Vereinbarung<br/></span> : ''}
          Tel.: <a href={`tel:${store.phone}`} className="store-phone">{store.phone}</a>
        </div>
      </div>
    )
  }

  renderBlockStore(store, index, offset) {
    return (
      <div key={index} className={`col-md-4 col-md-offset-${index % 2 === 0 ? offset : 0} col-sm-12`}>
        <div className="box wow fadeIn store-box" data-wow-duration="1.5s">
          <div className="store-textbox">
            <h4>{store.address.city}</h4>
            <div>
              {store.name}<br/>
              {store.address.street}<br/>
              {store.address.postcode} {store.address.city}<br/><br/>
            </div>
            {this.renderOpeningHours(store)}
          </div>
        </div>
      </div>
    )
  }

  renderBlock() {
    const offset = Math.abs((12 - 4 * this.state.stores.length) / 2)
    return (
      <div className="container">
        <div className="row stores">
        { this.state.stores.map((store, index) => this.renderBlockStore(store, index, offset)) }
        </div>
      </div>
    )
  }

  renderInlineStore(store, index) {
    return (
      <div key={index} className="row" style={{ paddingBottom: "20px" }}>
        <div className="col-sm-12 col-md-5">
          {store.name}<br/>
          {store.address.street}<br/>
          {store.address.postcode} {store.address.city}<br/><br/>
        </div>
        <div className="col-sm-12 col-md-7">
          {this.renderOpeningHours(store)}
        </div>
      </div>
    )
  }

  renderInline() {
    return (
      <div>
        {this.state.stores.map((store, index) => this.renderInlineStore(store, index))}
      </div>
    )
  }

  render() {
    switch (this.props.mode) {
      case 'inline':
        return this.renderInline()
      case 'block':
        return this.renderBlock()
      default:
        return <div></div>
    }
  }
}

export default Stores
