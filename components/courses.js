'use strict'

import React from 'react'

import RegisterBtn from './register-btn'

function cmp(tags, course) {
  return tags.indexOf(course.tag) > -1
}

class Courses extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      courses: []
    }
  }

  componentDidMount() {
    $.getJSON('/courses.json', (data) => {
      this.setState({ courses: data })
    });
  }

  static renderCourseDay(day, idx) {
    const date = new Date(day.date)
    return (
      <div key={idx} className="row course-row">
        <div className="col-md-1 col-md-offset-2 col-sm-3 col-xs-4">{date.toLocaleDateString()}</div>
        <div className="col-md-2 col-sm-3 col-xs-4">{date.toLocaleDateString('de-DE', { weekday: 'long' })}</div>
        <div className="col-md-2 col-sm-3 col-xs-4">{day.timeFrom}</div>
        <div className="col-md-2 col-sm-3 col-xs-4">{day.timeTo}</div>
        <div className="col-md-2 col-sm-3 col-xs-4">{day.lesson}</div>
        <div className="col-md-1 col-sm-3 col-xs-4">{day.instructor}</div>
      </div>
    )
  }

  renderCourseHeader(course) {
    {
      if (course.filename) {
        return (
          <div>
            <div className="col-xs-12 course-title">
              <h3 className="text-center">{course.title}</h3>
            </div>
            <div className="col-xs-12 col-md-3">
              <img src={`/images/courses/${course.filename}`} className="img-responsive"/>
            </div>
            <div className="col-xs-12 col-md-9">
              <div className="text-center" dangerouslySetInnerHTML={{ __html: course.description }}></div>
            </div>
          </div>
        )
      } else {
        return (
          <div>
            <div className="col-xs-12 course-title">
              <h3 className="text-center">{course.title}</h3>
            </div>
            <div className="col-xs-12">
              <div className="text-center" dangerouslySetInnerHTML={{ __html: course.description }}></div>
            </div>
          </div>
        )
      }
    }
  }

  renderCourse(course, idx) {
    return (
      <div key={idx} className="course-container">
        <div className="row course-header">
          {this.renderCourseHeader(course)}
        </div>
        { course.courseDays.map((day, idx) => Courses.renderCourseDay(day, idx)) }
        <div className="row course-footer">
          <div className="col-md-12 text-center">
            <RegisterBtn/>
          </div>
        </div>
      </div>
    );
  }

  renderStore(store, courses) {
    const storeCourses = courses.filter(course => store === course.store.address.city).sort((lhs, rhs) => {
      const d1 = new Date(lhs.courseDays[0].date)
      const d2 = new Date(rhs.courseDays[0].date)
      if (d1.getTime() === d2.getTime()) return 0
      else if (d1 < d2) return -1
      else return 1
    })
    return (
      <div key={store} className="courses-storebox">
        <h2 className="text-center">{store}</h2>
        { storeCourses.map((course, idx) => this.renderCourse(course, idx)) }
      </div>
    )
  }

  render() {
    let courses = this.state.courses
    if (this.props.tags) {
      const taggedCourses = this.state.courses.filter(course => this.props.tags.indexOf(course.tag) > -1)
      if (taggedCourses.length === 0) {
        return null
      }
      courses = taggedCourses
    }

    const stores = []
    courses.forEach(course => {
      if (stores.indexOf(course.store.address.city) === -1) stores.push(course.store.address.city)
    })

    return (
      <section style={this.props.style}>
        <div className="container">
          { stores.map(store => this.renderStore(store, courses)) }
        </div>
      </section>
    )
  }
}

export default Courses