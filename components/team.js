'use strict'

import React from 'react'
import '../resources/css/styles.css'

const DefinedRoles = {
  owner: 'Inhaber/in',
  manager: 'Geschäftsführer/in',
  teacher: 'Fahrlehrer/in',
  office: 'Büro'
};

export default class Team extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      team: []
    }
  }

  componentDidMount() {
    $.getJSON('/drivingSchool.json', (data) => {
      this.setState({ team: data.team })
    });
  }

  renderRoles(roles) {
    return (
      <span className="pull-sm-right">
      {
        roles.map((role, idx) => {
          const style = roles.length - 1 > idx ? { paddingRight: '10px'} : {}
          return <span key={`r${idx}`} style={style}>{DefinedRoles[role]}</span>
        })
      }
      </span>
    );
  }

  renderDrivingLicences(drivingLicences) {
    return (
      <div className="text-sm-center">
        {
          drivingLicences.map((drivingLicence) => `${drivingLicence} `)
        }
      </div>
    );
  }

  render() {
    return (
      <div className="row">
        {
          this.state.team.map((member, idx) => {
            const image = member.filename ? <img className="img-responsive" src={`/images/team/${member.filename}`} /> : null;
            return (
              <div className="col-sm-6 col-xs-12" key={idx}>
                <div style={{ padding: '10px' }}>
                  <div className={this.props.cssClass} style={this.props.style}>
                    { image }
                    <div className="col-xs-12 col-sm-6">{member.name}</div>
                    <div className="col-xs-12 col-sm-6">{this.renderRoles(member.roles)}</div>
                    <div className="col-xs-12">{this.renderDrivingLicences(member.drivingLicences)}</div>
                    <div className="col-xs-12 col-sm-8"><a href={`mailto:${member.emailAddress}`}>{member.emailAddress}</a></div>
                    <div className="col-xs-12 col-sm-4"><span className="pull-sm-right">{member.phone}</span></div>
                  </div>
                </div>
              </div>
            )
          })
        }
      </div>
    );
  }
}