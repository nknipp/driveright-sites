import React from 'react'
import Link from "gatsby-link"

export default ({ classes, title }) =>
  <div className="button register-btn" style={{ marginTop: '40px' }}>
    <Link to="/kontakt" className={`btn ${classes ? classes : ''}`}>{title ? title : 'Jetzt kontaktieren'}</Link>
  </div>
