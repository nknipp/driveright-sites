import React from 'react'

export default class Cars extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      cars: []
    }
  }

  componentDidMount() {
    $.getJSON('/drivingSchool.json', (data) => {
      this.setState({ cars: data.cars })
    });
  }

  render() {
    return (
      <div className="row">
        {
          this.state.cars.map((car, idx) => {
            const image = car.filename ? <img className="img-responsive" src={`/images/cars/${car.filename}`} /> : null;
            return (
              <div className="col-sm-12 col-md-6" key={idx}>
                <div className="box wow fadeIn" data-wow-duration="1.5s">
                  { image }
                  <div className="cars-textbox">
                    <div style={{ padding: '10px' }} dangerouslySetInnerHTML={{ __html: car.description }}></div>
                  </div>
                </div>
              </div>
            )
          })
        }
      </div>
    );
  }
}