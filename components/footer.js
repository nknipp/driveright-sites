import React from 'react'
import Link from "gatsby-link"

export default () => {
  return (
    <footer id="footer" className="text-center">
      <div className="container">
        <div className="row">
          <div className="col-md-12 col-sm-12">
            <div className="social">
              <Link to="/impressum">Impressum</Link>
              <Link to="/datenschutz">Datenschutz</Link>
            </div>
          </div>
          <div className="col-md-12 col-sm-12">
            <div className="copyright">
              <p className="wow zoomIn" data-wow-duration="1s">Made with <i className="fa fa-heart"></i> by <a href="https://www.drive-right.de">driveright</a>. {new Date().getFullYear()}. All Rights Reserved</p>
            </div>
          </div>
        </div>
      </div>
    </footer>
    )
}
