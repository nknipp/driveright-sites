'use strict'

import React from 'react'

class Impressum extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      drivingSchool: {
        address: {}
      }
    }
  }

  componentDidMount() {
    $.getJSON('/drivingSchool.json', (data) => {
      this.setState({ drivingSchool: data })
    });
  }

  render() {
    return (
      <section id="introduction" style={{ paddingTop: '180px' }}>
        <div className="container">
          <div className="heading">
            <div className="row">
              <div className="col-sm-8 col-sm-offset-2">
                <h3>Impressum</h3>
                <p><strong>Verantwortlich im Sinne von §6TDG</strong></p>
                <p><strong>{this.state.drivingSchool.name}</strong><br/>
                  Inhaber: {this.state.drivingSchool.owner}<br/>
                  {this.state.drivingSchool.managingDirector ? <span>Geschäftsführer: {this.state.drivingSchool.managingDirector}<br/></span> : ''}
                  {this.state.drivingSchool.address.street}<br/>
                  {this.state.drivingSchool.address.postcode} {this.state.drivingSchool.address.city}
                </p>

                <p><strong>Tel.:</strong> {this.state.drivingSchool.phone}</p>
                {this.state.drivingSchool.fax ? <p><strong>Fax:</strong> {this.state.drivingSchool.fax}</p> : ''}

                <p><strong>Web:</strong> {this.state.drivingSchool.website}</p>
                <p><strong>E-Mail:</strong> {this.state.drivingSchool.emailAddress}</p>

                {this.state.drivingSchool.commercialRegister ? <p><strong>Handelsregister:</strong> {this.state.drivingSchool.commercialRegister}</p> : ''}

                {this.state.drivingSchool.taxNumber ? <p><strong>Steuernummer:</strong> {this.state.drivingSchool.taxNumber}</p> : ''}

                {this.state.drivingSchool.taxIdNumber ? <p><strong>Umsatzsteuer-Identifikationsnummer:</strong> {this.state.drivingSchool.taxIdNumber}</p> : ''}

                {this.state.drivingSchool.operativeManager ? <p><strong>Verantwortlicher Leiter:</strong> {this.state.drivingSchool.operativeManager}</p> : ''}
                <p>
                  <strong>Aufsichtsbehörde:</strong><br/>
                  <pre style={{ padding: 'inherit', fontSize: 'inherit', lineHeight: 'inherit', color: 'inherit', backgroundColor: 'inherit', border: 'none', fontFamily: 'inherit' }}>{this.state.drivingSchool.regulatingAuthority}</pre>
                </p>

                <p>
                  <strong>Informationen auf unserer Website</strong><br/>
                  Das Informationsangebot der {this.state.drivingSchool.name} ist unverbindlich und wird fortlaufend überarbeitet und aktualisiert. Bitte haben Sie
                  Verständnis dafür, dass die {this.state.drivingSchool.name} sich vorbehalten müssen, jederzeit ohne vorherige Ankündigung das Angebot zu verändern,
                  zu ergänzen, zu löschen oder die Veröffentlichung einzustellen.
                </p>

                <p>
                  Verantwortlich gemäß § 55 Absatz 2 RfStV:<br/>
                  {this.state.drivingSchool.owner ? this.state.drivingSchool.owner : this.state.drivingSchool.name}<br/>
                  {this.state.drivingSchool.address.street}<br/>
                  {this.state.drivingSchool.address.postcode} {this.state.drivingSchool.address.city}
                </p>

                <p>
                  <strong>Online-Streitbeilegung</strong><br/>
                  Bei Problemen mit Verbrauchern können diese außergerichtlich auf der von der EU bereitgestellten Plattform gelöst werden.<br/>
                  <a href="https://ec.europa.eu/consumers/odr/">ec.europa.eu/consumers/odr</a>
                </p>

                <p>
                  <strong>Haftung für Inhalte</strong><br/>
                  Die {this.state.drivingSchool.name} aktualisiert und überarbeitet fortlaufend die Informationen auf der Webseite. Trotz aller Sorgfalt können sich
                  Fakten kurzfristig ändern. Eine Haftung oder Garantie für die jederzeitige Aktualität, Richtigkeit und Vollständigkeit der
                  zur Verfügung gestellten Informationen kann daher nicht übernommen werden. Die {this.state.drivingSchool.name} ist gemäß § 7 Absatz 1 TMG für eigene
                  Inhalte auf unseren Webseiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG ist die {this.state.drivingSchool.name} nicht
                  verpflichtet, übermittelte oder gespeicherte Fremdinformationen zu überwachen oder nach Umständen zu forschen, die auf eine
                  rechtswidrige Tätigkeit hinweisen. Die Verpflichtung zur Entfernung oder Sperrung der Nutzung von Informationen nach den
                  allgemeinen Gesetzen bleibt hiervon unberührt. Die {this.state.drivingSchool.name} haftet für solche fremden Informationen erst, wenn Kenntnis von einer
                  rechtswidrigen Handlung oder einer Information besteht und wenn im Falle von Schadensersatzansprüchen, Tatsachen oder Umstände
                  bekannt sind, aus denen die rechtswidrige Handlung oder Information offensichtlich wird, es sei denn, die {this.state.drivingSchool.name} wäre unverzüglich
                  tätig geworden, nachdem Kenntnis erlangt wurde, um die Informationen zu entfernen und den Zugang zu sperren. Bei Bekanntwerden
                  nachgewiesener Rechtsverletzungen werden solche Inhalte umgehend entfernen. Sollten Sie auf eine aus Ihrer Sicht unrichtige,
                  veraltete oder unvollständige Information aufmerksam werden, stellen Sie bitte eine entsprechende Kontaktaufnahme.
                </p>

                <p>
                  <strong>Haftung für Links</strong><br/>
                  Das Angebot der {this.state.drivingSchool.name} enthält Links zu externen Webseiten Dritter, auf deren Inhalte kein Einfluss besteht. Für die Inhalte der
                  verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber dieser Seiten verantwortlich. Die verlinkten Seiten wurden zum
                  Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht
                  erkennbar. Grundsätzlich macht sich die {this.state.drivingSchool.name} den Inhalt der Webseiten, zu denen verlinkt wird, nicht zu eigen, sondern verweist
                  ähnlich einer Fußnote auf weitergehende Informationen zu dem jeweiligen Thema. Eine permanente inhaltliche Kontrolle der verlinkten
                  Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen wird
                  die {this.state.drivingSchool.name} derartige Links nach Prüfung des Sachverhalts umgehend entfernen. Sollte Ihnen eine Rechtsverletzung auf von {this.state.drivingSchool.name} verlinkte
                  Websites Dritter auffallen, stellen Sie bitte eine entsprechende Kontaktaufnahme.
                </p>

                <p>
                  <strong>Urheberrecht</strong><br/>
                  Alle Rechte vorbehalten. Die Webseite der {this.state.drivingSchool.name} bietet eine Vielzahl von Informationen, die regelmäßig aktualisiert werden. Alle auf
                  der Webseite der {this.state.drivingSchool.name} erscheinenden Werke, insbesondere Texte, Fotografien, Grafiken, Filme etc. unterliegen dem deutschen Urheberrecht
                  und/oder dem deutschen Wettbewerbsrecht. Jegliche Nutzung, insbesondere die Vervielfältigung und Verbreitung in gedruckter oder
                  elektronischer Form, die Vorführung und Ausstellung, die öffentliche Zugänglichmachung und die sonstige öffentliche Wiedergabe
                  einschließlich der Bearbeitung und Nutzung von Bearbeitungen sowie der Lizenzierung, Vermietung sowie sonstigen Übertragung von
                  Nutzungsrechten ist ohne unsere schriftliche Zustimmung untersagt, es sei denn es bestünde ausnahmsweise eine gesetzliche Erlaubnis.
                  Downloads und Kopien dieser Webseite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser
                  Webseite nicht von der {this.state.drivingSchool.name} erstellt wurde, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche
                  gekennzeichnet. Sollten Sie auf einen Umstand aufmerksam werden, der auf eine Urheberrechtsverletzung hindeuten könnte, stellen Sie bitte
                  eine entsprechende Kontaktaufnahme. Bei Bekanntwerden von Rechtsverletzungen wird die {this.state.drivingSchool.name} derartige Inhalte nach Prüfung umgehend entfernen.
                </p>

                <p>
                  <strong>Datenschutz</strong><br/>
                  Die {this.state.drivingSchool.name} nimmt den Schutz Ihrer persönlichen Daten sehr ernst. Daher werden Ihre personenbezogenen Daten vertraulich und entsprechend der
                  gesetzlichen Datenschutzvorschriften behandelt.
                </p>

                <p>
                  <strong>Kontaktformular</strong><br/>
                  Wenn Sie der {this.state.drivingSchool.name} per Kontaktformular Anfragen zukommen lassen, werden Ihre Angaben aus dem Anfrageformular inklusive der von Ihnen dort angegebenen
                  Kontaktdaten zwecks Bearbeitung der Anfrage und für den Fall von Anschlussfragen gespeichert. Diese Daten werden nicht ohne Ihre Einwilligung weitergegeben.
                </p>

                <p>
                  <strong>Newsletter</strong><br/>
                  Wenn Sie den auf der Webseite angebotenen Newsletter beziehen möchten, benötigt die {this.state.drivingSchool.name} von Ihnen eine E-Mail Adresse sowie Informationen, welche die
                  Überprüfung gestattet, dass Sie der Inhaber der angegebenen E-Mail Adresse sind und mit dem Empfang des Newsletters einverstanden sind. Weitere Daten werden
                  nicht erhoben. Diese Daten werden ausschließlich für den Versand der angeforderten Informationen verwendet und werden nicht an Dritte weitergegeben. Die
                  erteilte Einwilligung zur Speicherung der Daten, der E-Mail Adresse sowie deren Nutzung zum Versand des Newsletters können Sie jederzeit widerrufen, etwa
                  über den "Austragen"-Link im Newsletter.
                </p>

                <p>
                  <strong>Google Analytics</strong><br/>
                  Diese Website nutzt Funktionen des Webanalysedienstes Google Analytics. Anbieter ist die Google Inc., 1600 Amphitheatre Parkway Mountain View, CA 94043, USA.
                  Google Analytics verwendet so genannte "Cookies". Das sind Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Webseite
                  durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA
                  übertragen und dort gespeichert. Mehr Informationen zum Umgang mit Nutzerdaten bei Google Analytics finden Sie in der Datenschutzerklärung von Google:
                  https://support.google.com/analytics/answer/6004245?hl=de - Browser Plugin: Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer
                  Browser-Software verhindern; die {this.state.drivingSchool.name} weist Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich
                  werden nutzen können. Sie können darüber hinaus die Erfassung der durch den Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse)
                  an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem Sie das unter dem folgenden Link verfügbare Browser-Plugin herunterladen und installieren:
                  https://tools.google.com/dlpage/gaoptout?hl=de - Widerspruch gegen Datenerfassung: Sie können die Erfassung Ihrer Daten durch Google Analytics verhindern, indem Sie
                  auf folgenden Link klicken. Es wird ein Opt-Out-Cookie gesetzt, der die Erfassung Ihrer Daten bei zukünftigen Besuchen dieser Website verhindert: Google Analytics deaktivieren
                </p>

                <p>
                  <strong>Widerspruch Werbe-Mails</strong><br/>
                  Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien
                  wird hiermit widersprochen. Die {this.state.drivingSchool.name} behält sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-E-Mails, vor.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

export default Impressum
