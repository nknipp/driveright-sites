'use strict'

import React from 'react'
import Rating from 'react-rating'

class FbRatingStatistics extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      facebookId: 0,
      ratingsCount: 0,
      overallStarRating: 0
    }
  }

  componentDidMount() {
    $.getJSON('/rating-statistics.json', (data) => {
      this.setState({
        facebookId: data.id,
        ratingsCount: data.rating_count,
        overallStarRating: data.overall_star_rating
      })
    });
  }

  render() {
    if (!this.state.facebookId) return <span></span>

    return (
      <span className="navbar-rating">
        <a href={`https://www.facebook.com/${this.state.facebookId}/reviews`} title="Bewertungen bei facebook">
          {this.state.overallStarRating} von 5 bei {this.state.ratingsCount} Bewertungen<br/>
          <span><i className="fa fa-facebook-square fa-2x"></i></span>&nbsp;&nbsp;
          <Rating
            readonly={true}
            fractions={10}
            initialRate={this.state.overallStarRating}
            empty="fa fa-star-o fa-2x rating-empty"
            full="fa fa-star fa-2x rating-full"
          />
        </a>
      </span>
    )
  }
}

export default FbRatingStatistics