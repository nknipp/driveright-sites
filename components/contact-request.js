'use strict'

import React from 'react'

class ContactRequest extends React.Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
    this.handleCheck = this.handleCheck.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this)
    this.state = {
      name: '',
      emailAddress: '',
      mobile: '',
      interestedIn: '',
      message: ''
    }
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleCheck() {
    let interestedInVal = '';
    const cbs = $('#contactRequestForm').find('input:checked');
    cbs.each(function (idx, elem) {
      interestedInVal += $(elem).val() + ' ';
    })
    this.setState({
      interestedIn: interestedInVal.trim()
    });
  }

  handleSubmit() {
    const msg = encodeURIComponent(this.state.message)
    const serializedData = `{
      "query": "mutation { sendContactRequest(name: \\"${this.state.name}\\", mobile: \\"${this.state.mobile}\\", emailAddress: \\"${this.state.emailAddress}\\", subject: \\"${this.state.interestedIn}\\", message: \\"${msg}\\" ) { success errors }}",
      "variables": null
    }`
    $.ajax({
      url: "https://api.drive-right.de/q",
      method: "POST",
      contentType: "application/json",
      data: serializedData,
      success: function(data) {
        if (!data.data.sendContactRequest.success) {
          let errorMessage = '<ul>';
          data.data.sendContactRequest.errors.forEach(function(err) {
            errorMessage += '<li>' + err + '</li>';
          });
          errorMessage += '</ul>';
          $('#contactRequestMessages').show().html(errorMessage);
        } else {
          $('#contactRequestMessages').hide().html('');
          $('#contactRequestForm').hide();
          $('#contactRequestSuccess').show();
        }
      }
    });
  }

  render() {
    return (
      <div className="contact-form wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
          <div className="row">
            <div className="col-sm-12">
              <form id="contactRequestForm" name="contactRequestForm" method="post" action="#">
                <div id="contactRequestMessages" style={{ display: "none" }} className="col-sm-10"></div>
                <div className="row  wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                  <div className="form-group">
                    <input type="text" name="name" className="form-control" placeholder="Vor- und Zuname" required="required" onChange={this.handleChange}/>
                  </div>
                  <div className="form-group">
                    <input type="email" name="emailAddress" className="form-control" placeholder="E-Mail Adresse" required="required" onChange={this.handleChange}/>
                  </div>
                  <div className="form-group">
                    <input type="tel" name="mobile" className="form-control" placeholder="Mobil" required="required" onChange={this.handleChange}/>
                  </div>
                  <div className="form-group interested-in">
                    <div className="col-sm-12 col-md-12">
                      <label>Ich interessiere mich für ...</label>
                    </div>
                    <div className="col-sm-12 col-md-4">
                      <input type="checkbox" name="interestedIn" value="Auto" onClick={this.handleCheck} style={{ width: "20px", height: "20px" }}/> <label style={{ verticalAlign: "bottom" }}>Auto</label>
                    </div>
                    <div className="col-sm-12 col-md-4">
                      <input type="checkbox" name="interestedIn" value="Motorrad" onClick={this.handleCheck} style={{ width: "20px", height: "20px" }}/> <label style={{ verticalAlign: "bottom" }}>Motorrad</label>
                    </div>
                    <div className="col-sm-12 col-md-4">
                      <input type="checkbox" name="interestedIn" value="LKW oder Bus" onClick={this.handleCheck} style={{ width: "20px", height: "20px" }}/> <label style={{ verticalAlign: "bottom" }}>LKW oder Bus</label>
                    </div>
                    <div className="col-sm-12 col-md-4">
                      <input type="checkbox" name="interestedIn" value="Sonstiges" onClick={this.handleCheck} style={{ width: "20px", height: "20px" }}/> <label style={{ verticalAlign: "bottom" }}>Sonstiges</label>
                    </div>
                    <div className="col-sm-12 col-md-6">
                      <input type="checkbox" name="drivingInstructor" value="Fahrlehrer/in gesucht" onClick={this.handleCheck} style={{ width: "20px", height: "20px" }}/> <label style={{ verticalAlign: "bottom" }}>Fahrlehrer/in gesucht</label>
                    </div>
                  </div>
                  <div className="form-group">
                    <textarea name="message" className="form-control" rows="8" placeholder="Nachricht" required="required" onChange={this.handleChange}></textarea>
                  </div>
                  <div className="form-group">
                    <button type="button" className="btn btn-primary btn-lg" onClick={this.handleSubmit}>Absenden</button>
                  </div>
                </div>
              </form>
              <div id="contactRequestSuccess" className="contact-request-success" style={{ display: "none" }}>
                <h3>Vielen Dank für ihre Anfrage. Wir werden uns sofort darum kümmern.</h3>
              </div>
            </div>
          </div>
        </div>
    )
  }
}

export default ContactRequest
